package denote.message_handlers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import denote.junit.InMemoryConnection;
import denote.junit.MessageNoteBuilder;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.impl.message.GenericMessage;
import denote.response.api.IResponse;
import denote.response.api.Type;

public class NoteHandlerTest
{
    @BeforeClass
    public static void setConn() throws Exception
    {
        InMemoryConnection.getConnection();
    }

    @AfterClass
    public static void closeConn() throws Exception
    {
        new GenericMessage(Domain.DB, Action.CLOSE).execute();
    }

    @Test
    public void saving_a_note_without_categories_will_not_fail() throws Exception
    {
        GenericMessage msg = MessageNoteBuilder.aSaveMessage()
            .withNoCategs()
            .withBody("body")
            .withTitle("title")
            .build();
        
        IResponse resp = new NoteHandler().handle(msg);
        
        // integer means a valid Note ID, thus the note has been saved
        assertThat(resp.getType().toString(), is(Type.INTEGER.toString()));
    }
}

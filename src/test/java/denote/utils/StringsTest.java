package denote.utils;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Test;

public class StringsTest 
{
	@Test
	public void 
	should_return_true_on_truthy_strings_using_is_truthy() 
	{
		String[] truthyStrings = new String[] { "true", " true ", "1", " 1", "1 ", "foobar" };
		for (String truthyString : truthyStrings) {
			assertThat(Strings.isTruthy(truthyString), is(true));
		}
	}

	@Test
	public void 
	should_return_false_on_falsy_strings_using_is_truthy() 
	{
		String[] truthyStrings = new String[] { "", "   ", "0", " 0", "0 ", null, "\n" };
		for (String truthyString : truthyStrings) {
			assertThat(Strings.isTruthy(truthyString), is(false));
		}
	}
	
	@Test
	public void 
	should_return_proper_values_for_blank_and_not_blank() throws Exception 
	{
		String[] blankValues = new String[] {null, "", "  ", "\n", "\t", "\n\t\r"};
		String[] nonBlankValues = new String[] {"1", "0", "false", "  1", "1  ", " foobar "};
		
		for (String blankValue : blankValues) {
			assertThat(Strings.isBlank(blankValue), is(true));
		}
		
		for (String nonBlankValue : nonBlankValues) {
			assertThat(Strings.isNotBlank(nonBlankValue), is(true));
		}		
	}
	
	@Test
	public void 
	ltrim_should_work() throws Exception 
	{
		assertThat(Strings.ltrim(" foobar"), is("foobar"));
		assertThat(Strings.ltrim(" foobar "), is("foobar "));
		assertThat(Strings.ltrim("\rfoobar"), is("foobar"));
		assertThat(Strings.ltrim("\r\nfoobar"), is("foobar"));
		assertThat(Strings.ltrim("foobar"), is("foobar"));
		assertThat(Strings.ltrim("foobar  "), is("foobar  "));
	}
	
	@Test
	public void 
	rtrim_should_work() throws Exception 
	{
		assertThat(Strings.rtrim(" foobar"), is(" foobar"));
		assertThat(Strings.rtrim(" foobar "), is(" foobar"));
		assertThat(Strings.rtrim("\rfoobar"), is("\rfoobar"));
		assertThat(Strings.rtrim("\r\nfoobar"), is("\r\nfoobar"));
		assertThat(Strings.rtrim("foobar"), is("foobar"));
		assertThat(Strings.rtrim("foobar  "), is("foobar"));
		assertThat(Strings.rtrim("\r\nfoobar\r\n"), is("\r\nfoobar"));
	}
	
	@Test
    public void 
    should_join_array_list_with_delimiter() throws Exception
    {
	    assertThat(Strings.join(",", Arrays.asList("1","2","3").iterator()), is("1,2,3"));
	    assertThat(Strings.join(",", Arrays.asList("").iterator()), is(""));
    }
}

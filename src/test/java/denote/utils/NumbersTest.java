package denote.utils;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;


public class NumbersTest 
{
	@Test
	public void should_return_proper_values_on_valid_or_invalid_integers() throws Exception 
	{
		String[] validInts = new String[] {"1", "2", "-10", "600", "456777888", "0", "-255"};
		
		for (String validInt : validInts) {
			assertThat(Numbers.isInteger(validInt), is(true));
			assertThat(Numbers.isNotNullInteger(validInt), is(true));
		}
		
		assertThat(Numbers.isInteger("1.2"), is(false));
		assertThat(Numbers.isInteger("-1.45"), is(false));
		assertThat(Numbers.isInteger("0.5"), is(false));
		
		assertThat(Numbers.isNotNullInteger("1.2"), is(false));
		assertThat(Numbers.isNotNullInteger("-1.45"), is(false));
		assertThat(Numbers.isNotNullInteger("0.5"), is(false));		
	}
	
	@Test(expected = NullPointerException.class)
	public void should_throw_npe_on_null() throws Exception 
	{
		Numbers.isInteger(null);
	}
	
	@Test
	public void should_not_throw_npe_when_using_null_with_check_method() throws Exception 
	{
		try {
			assertThat(Numbers.isNotNullInteger(null), is(false));
		} catch (NullPointerException npe) {
			fail("Should not throw NPE");
		}
	}	
}

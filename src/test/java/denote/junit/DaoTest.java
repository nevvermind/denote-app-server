package denote.junit;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import denote.db.ModelDaoAbstract;

public abstract class DaoTest
{
    protected static Connection CONN;

    abstract public <T extends ModelDaoAbstract<?>> T getTestedDao();
    
    @BeforeClass
    public static void startDbConn() throws Exception
    {
        CONN = InMemoryConnection.getConnection();
        assertThat(CONN, is(notNullValue()));
    }
    
    @AfterClass
    public static void closeDbConn() throws Exception
    {
        InMemoryConnection.closeConnection();
        CONN = null;
    }    
    
    @After
    public void truncateDaoTable() throws SQLException
    {
        CONN.prepareStatement("DELETE FROM " + getTestedDao().getTableName()).execute();
        ResultSet rs = CONN.prepareStatement(String.format("SELECT COUNT(*) FROM %s", getTestedDao().getTableName())).executeQuery();
        rs.next();
        assertThat("Table " + getTestedDao().getTableName() + " should not have any rows", rs.getInt(1), is(0));
    }    
}

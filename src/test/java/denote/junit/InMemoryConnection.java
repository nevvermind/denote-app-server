package denote.junit;

import java.sql.Connection;
import java.sql.SQLException;

import com.google.gson.JsonObject;

import denote.Args;
import denote.Config;
import denote.Registry;
import denote.exception.ConfigurationException;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.impl.message.GenericMessage;
import denote.message_handlers.DbHandler;

public class InMemoryConnection
{
    public static Connection getConnection() throws SQLException, ConfigurationException
    {
        Registry.getInstance().set("config", new ConfigTest());
        
        JsonObject params = new JsonObject();
        params.addProperty("path", ":memory:");
        GenericMessage msg = new GenericMessage(Domain.DB, Action.OPEN, params);
        new DbHandler().handle(msg);
        return (Connection) Registry.getInstance().get(DbHandler.REGKEY_DB);
    }
    
    public static void closeConnection() throws SQLException
    {
        new GenericMessage(Domain.DB, Action.CLOSE).execute();
    }
    
    private static class ConfigTest extends Config 
    {
        public ConfigTest() throws ConfigurationException
        {
            super(new Args(new String[] {}));
        }
    }    
}

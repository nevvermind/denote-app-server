package denote.junit;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetHelper
{
    public static int getNumberOfRows(ResultSet rs) throws SQLException
    {
        int counter = 0;
        while (rs.next()) {
            counter++;
        }
        return counter;
    }
}

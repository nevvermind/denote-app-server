package denote.junit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.impl.message.GenericMessage;

public class MessageNoteBuilder
{
    private JsonObject params = new JsonObject();
    private JsonArray categs = new JsonArray();
    
    public static MessageNoteBuilder aSaveMessage()
    {
        return new MessageNoteBuilder();
    }
    
    public MessageNoteBuilder withTitle(String title)
    {
        params.addProperty("title", title);
        return this;
    }
    
    public MessageNoteBuilder withBody(String body)
    {
        params.addProperty("body", body);
        return this;
    }
    
    public MessageNoteBuilder withCateg(String name)
    {
        categs.add(new JsonPrimitive(String.format("{name:'%s'}", name)));
        return this;
    }
    
    public MessageNoteBuilder withCateg(int categId)
    {
        categs.add(new JsonPrimitive(categId));
        return this;
    }
    
    private boolean noCategs;
    
    public MessageNoteBuilder withNoCategs()
    {
        noCategs = true;
        return this;
    }
    
    public GenericMessage build()
    {
        if (!noCategs) {
            params.add("categs", categs);
        }
        return new GenericMessage(Domain.NOTE, Action.SAVE, params);
    }
}
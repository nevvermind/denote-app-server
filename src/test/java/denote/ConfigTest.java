package denote;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import denote.exception.ConfigurationException;

public class ConfigTest
{
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();    

    @Test
    public void configFileIsReadFromCliArg() throws IOException
    {
        File config1File = testFolder.newFile("config1.ini");
        
        PrintWriter fileOut = new PrintWriter(config1File);
        fileOut.println("foobar=bar");
        fileOut.println("foobaz=baz");
        fileOut.println("config1=1");
        fileOut.println("config2=true");
        fileOut.println("config3=True");
        fileOut.close();
        
        try {
            new App(new String[] { "--config=" + config1File.getAbsolutePath() });
        } catch (ConfigurationException e) {
            fail("Configuration setup failed");
        }
        
        assertThat((String) Registry.getConfig().getConfig().get("foobar"), is("bar"));
        assertThat((String) Registry.getConfig().getConfig().get("foobaz"), is("baz"));
        assertThat(Registry.getConfig().getInt("config1", 0), is(1));
        assertThat("Config 'configXXX' doesn't exists so the Config should return 666", Registry.getConfig().getInt("configXXX", 666), is(666));
        assertThat("A bool true is returned only on 'true' string", Registry.getConfig().getBool("config1", false), is(false)); // TODO - strange behavior
        assertThat("Wierd but true: The default value of getBool() regards only if the config property is null", Registry.getConfig().getBool("config1", true), is(false));
        assertThat(Registry.getConfig().getBool("config2", false), is(true));
        assertThat("Case sesitive check: 'True' != 'true'", Registry.getConfig().getBool("config3", true), is(false));
    }
}

package denote;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;


public class ArgsTest
{
    @Test
    public void argsFormat()
    {
        Args args = new Args(new String[] {
            "--arg1", "--arg2=val2", "--arg3=val3", "--arg4="
        });
        
        assertThat("Args should not be empty", true, is(args.hasArgs()));
        assertThat("No value args should return as empty strings", "", is(args.get("arg1")));
        assertThat("val3", is(args.get("arg3")));
        assertThat("val2", is(args.get("arg2")));
        assertThat("", is(args.get("arg4")));
        assertThat(null, is(args.get("arg5")));
        assertThat(false, is(args.hasArg("foobar")));
        assertThat(true, is(args.hasArg("arg1")));
        assertThat(true, is(args.hasArg("arg2")));
        assertThat(true, is(args.hasArg("arg4")));
    }
}

package denote;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

import denote.exception.ConfigurationException;


public class RegistryTest
{
    @Test
    public void cliModeOnANonInitializedApplicationReturnsFalse()
    {
        try {
            new App(new String[] {});
        } catch (ConfigurationException e) {
            fail("App failed initialization");
        }        
        assertThat(Registry.getInstance().get("foobar"), is(nullValue()));
        assertThat((boolean) Registry.getInstance().get("foobar", false), is(false));
        assertThat((String) Registry.getInstance().get("foobar", ""), is(""));
        
        Registry.getInstance().set("foobar", "baz");
        assertThat((String) Registry.getInstance().get("foobar"), is("baz"));
        assertThat((String) Registry.getInstance().get("foobar", "bar"), is("baz"));
    }
}

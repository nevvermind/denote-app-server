package denote.db;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class DatetimeTest
{
    @Test
    public void 
    should_validate_a_valid_date_format() throws Exception
    {
        assertThat(Datetime.validate("2007-01-01 10:00:00"), is(true));
        assertThat(Datetime.validate("2119-12-31 23:59:59"), is(true));
        assertThat(Datetime.validate("2012-02-29 23:59:59"), is(true));
        assertThat(Datetime.validate("2013-02-28 00:00:00"), is(true));
        assertThat(Datetime.validate("2015-02-27 01:00:00"), is(true));
    }
    
    @Test
    public void 
    should_not_validate_an_invalid_date_format() throws Exception
    {
        assertThat(Datetime.validate(" 2012-10-10 23:59:59 "), is(false));
        assertThat(Datetime.validate("2007-31-01 10:00:00"), is(false));
        assertThat(Datetime.validate("2007-10-1010:00:00"), is(false));
        assertThat(Datetime.validate("2007-10-09 23:59:60 "), is(false));
        assertThat(Datetime.validate("2007-10-09 23:60:00 "), is(false));
        assertThat(Datetime.validate("2007-10-09 24:00:00 "), is(false));
        assertThat(Datetime.validate(" 20007-10-09 23:59:00"), is(false));
        assertThat(Datetime.validate("2007-100-09 23:59:00"), is(false));
        assertThat(Datetime.validate("2007-10-009 23:59:00"), is(false));
        assertThat(Datetime.validate("2007-3x-01 10:00:0x"), is(false));
        assertThat(Datetime.validate("10-09 23:59:60"), is(false));
        assertThat(Datetime.validate("10-09"), is(false));
        assertThat(Datetime.validate("2007-31-01"), is(false));
        assertThat(Datetime.validate(" "), is(false));
        assertThat(Datetime.validate("foobar"), is(false));
        assertThat(Datetime.validate(null), is(false));
        assertThat(Datetime.validate("2013-02-29 00:00:00"), is(false));
        assertThat(Datetime.validate("2012-02-30 23:59:59"), is(false));
        assertThat(Datetime.validate("2015-02-29 01:00:00"), is(false));
    }
    
    @Test
    public void own_generated_datetime_string_should_validate() throws Exception
    {
        assertThat(Datetime.validate(Datetime.getDbCurrentTime()), is(true));
    }
}

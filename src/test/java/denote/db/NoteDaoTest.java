package denote.db;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.Test;

import denote.junit.DaoTest;
import denote.junit.ResultSetHelper;
import denote.models.Category;
import denote.models.Note;
import denote.models.NoteCategory;

public class NoteDaoTest extends DaoTest
{
    private static final NoteDao DAO = new NoteDao();
    private static final String TABLE_NAME = DAO.getTableName();
    
    @SuppressWarnings("unchecked")
    @Override
    public NoteDao getTestedDao()
    {
        return DAO;
    }

    private Note getValidNote() throws Exception
    {
        Note note = new Note();
        note.setBody("body");
        note.setTitle("title");
        note.setCreatedAt("2010-10-10 10:10:10");
        note.setUpdatedAt("2010-10-10 10:10:10");
        return note;
    }

    @Test
    public void new_note_is_saved() throws Exception
    {
        Note note = getValidNote();
        assertThat(note.getId(), is(0));
        DAO.save(note);
        assertThat(note.getId() > 0, is(true));
    }
    
    @Test
    public void changed_note_is_updated_on_save() throws Exception
    {
        Note note = getValidNote();
        DAO.save(note);
        int oldId = note.getId();
        assertThat(oldId > 0, is(true));
        assertThat(note.getTitle(), is("title"));
        assertThat(note.getBody(), is("body"));
        
        note.setTitle("title 2");
        note.setBody("body 2");
        DAO.save(note);
        assertThat(oldId == note.getId(), is(true));
        assertThat(note.getTitle(), is("title 2"));
        assertThat(note.getBody(), is("body 2"));
    }
    
    @Test
    public void deleting_a_note_should_remove_from_db() throws Exception
    {
        Note note = getValidNote();
        PreparedStatement ps = CONN.prepareStatement("SELECT * FROM " + TABLE_NAME);
        
        DAO.save(note);
        ResultSet rs = ps.executeQuery();
        assertThat(ResultSetHelper.getNumberOfRows(rs), is(1));
        
        DAO.delete(note);
        ResultSet rs1 = ps.executeQuery();
        assertThat(ResultSetHelper.getNumberOfRows(rs1), is(0));
    }
    
    @Test
    public void loading_a_note_works() throws Exception
    {
        Note note = getValidNote();
        DAO.save(note);
        
        Note existingNote = new Note();
        DAO.load(note.getId(), existingNote);
        
        assertThat(existingNote.getId(), is(note.getId()));
    }
    
    @Test
    public void loading_a_note_through_dao_does_not_load_its_category_too() throws Exception
    {
        Note note = getValidNote();
        DAO.save(note);
        
        Category categ = new Category("foobar categ");
        new CategoryDao().save(categ);
        
        new NoteCategoryDao().save(new NoteCategory(note.getId(), categ.getId()));
        
        Note existingNote = new Note();
        DAO.load(note.getId(), existingNote);
        
        assertThat(existingNote.getId(), is(note.getId()));
        assertThat(existingNote.getCategories(), hasSize(0));
    }    
}

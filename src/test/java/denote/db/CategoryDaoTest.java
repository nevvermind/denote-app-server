package denote.db;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterableOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import denote.junit.DaoTest;
import denote.junit.ResultSetHelper;
import denote.models.Category;

public class CategoryDaoTest extends DaoTest
{
    private static final CategoryDao DAO = new CategoryDao();
    private static final int CORRECT_COLUMN_COUNT = 3;
    private static final String CATEG_TABLE_NAME = DAO.getTableName();
    
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @SuppressWarnings("unchecked")
    @Override
    public CategoryDao getTestedDao()
    {
        return DAO;
    }    
    
    private Category getValidCategory()
    {
        Category childCateg = new Category();
        childCateg.setCreatedAt("2010-10-10 10:10:10");
        childCateg.setName("name");
        return childCateg;
    }
    
    private Category getValidCategory(String name)
    {
        Category childCateg = new Category();
        childCateg.setCreatedAt("2010-10-10 10:10:10");
        childCateg.setName(name);
        return childCateg;
    }    
    
    private ResultSet getDescribeTable() throws SQLException
    {
        return CONN.prepareStatement(String.format("PRAGMA table_info('%s')", CATEG_TABLE_NAME)).executeQuery();
    }
    
    @Test
    public void should_have_the_correct_number_of_columns() throws Exception
    {
        assertThat(
            String.format("Table should have %d columns", CORRECT_COLUMN_COUNT), 
            ResultSetHelper.getNumberOfRows(getDescribeTable()),
            is(CORRECT_COLUMN_COUNT)
        );
    }
    
    @Test
    public void 
    category_id_column_is_pk() throws Exception
    {
        ResultSet rs = getDescribeTable();
        boolean categColFound = false;
        boolean categIdColIsPk = false;
        while (rs.next()) {
            if (rs.getString("name").equals("category_id")) {
                categIdColIsPk = rs.getInt("pk") == 1;
                categColFound = true;
            }
        }
        
        assertThat("There is no 'category_id' column", true, is(categColFound));
        assertThat("The 'category_id' should be PK", true, is(categIdColIsPk));
    }
    
    @Test
    public void parent_id_column_can_be_null() throws Exception
    {
        ResultSet rs = getDescribeTable();
        boolean colFound = false;
        boolean canBeNull = false;
        while (rs.next()) {
            if (rs.getString("name").equals("parent_id")) {
                canBeNull = rs.getInt("notnull") == 0;
                colFound = true;
            }
        }
        
        assertThat("There is no 'parent_id' column", true, is(colFound));
        assertThat("The 'parent_id' should accept NULL values", true, is(canBeNull));
    }
    
    @Test
    public void only_the_category_id_column_should_be_pk() throws Exception
    {
        ResultSet rs = getDescribeTable();
        int pksFound = 0;
        
        while (rs.next()) {
            if (rs.getInt("pk") == 1) {
                ++pksFound;
            }
        }
        
        assertThat("Only the 'category_id' column should be PK", 1, is(pksFound));
    }
    
    @Test
    public void a_saved_model_should_have_an_id_when_saved() throws Exception
    {
        Category categ  = getValidCategory();
        Category categ2 = getValidCategory();        
        
        assertThat(0, is(categ.getId()));
        assertThat(0, is(categ2.getId()));
        
        DAO.save(categ);
        DAO.save(categ2);
        
        assertThat(true, is(categ.getId() > 0));
        assertThat(true, is(categ2.getId() > 0));
        assertThat(true, is(categ2.getId() != categ.getId()));
    }
    
    @Test
    public void dao_save_returns_a_category() throws Exception
    {
        assertThat(DAO.save(getValidCategory()), is(instanceOf(Category.class)));
    }
    
    @Test
    public void deleting_a_model_removes_the_row() throws Exception
    {
        Category categ = getValidCategory();
        
        DAO.save(categ); 
        assertThat(true, is(categ.getId() > 0));
        
        int categId = categ.getId();
        
        DAO.delete(categ);
        
        PreparedStatement ps = CONN.prepareStatement("SELECT category_id FROM " + CATEG_TABLE_NAME + " WHERE category_id = ?");
        ps.setInt(1, categId);
        ResultSet rs = ps.executeQuery();
        
        assertThat(ResultSetHelper.getNumberOfRows(rs), is(0));
    }
    
    @Test
    public void deletion_of_an_non_existent_but_numeric_category_id_returns_false() throws Exception
    {
        Category category = new Category();
        category.setId(1234);
        assertThat(false, is(DAO.delete(category)));
    }
    
    @Test(expected = IllegalStateException.class)
    public void deletion_of_a_category_with_a_null_id_throws() throws Exception
    {
        DAO.delete(new Category());
    }
    
    @Test
    public void move_category_updates_the_parent_id() throws Exception
    {
        Category parentCateg = getValidCategory();
        DAO.save(parentCateg);
        assertThat(true, is(parentCateg.getId() > 0));
        
        Category childCateg = getValidCategory();
        DAO.save(childCateg);
        assertThat(true, is(childCateg.getId() > 0));
        assertThat(0, is(childCateg.getParentId()));
        
        DAO.move(childCateg.getId(), parentCateg.getId());
        
        PreparedStatement ps = CONN.prepareStatement("SELECT parent_id FROM " + CATEG_TABLE_NAME + " WHERE category_id = ?");
        ps.setInt(1, childCateg.getId());
        ResultSet rs = ps.executeQuery();
        rs.next();
        
        assertThat(parentCateg.getId(), is(rs.getInt("parent_id")));
    }

    @Test
    public void move_category_to_a_numeric_but_nonexistent_parent_id_throws_a_fk_constraint_sql_exception() throws Exception
    {
        expectedEx.expect(SQLException.class);
        expectedEx.expectMessage("SQLITE_CONSTRAINT");
        expectedEx.expectMessage("FOREIGN KEY");
        
        Category categ = getValidCategory();
        DAO.save(categ);
        assertThat(true, is(categ.getId() > 0));
        DAO.move(categ.getId(), 1234);
    }    
    
    @Test
    public void rename_category_updates_the_name_column() throws Exception
    {
        Category categ = getValidCategory();
        categ.setName("barbaz");
        DAO.save(categ);
        assertThat(true, is(categ.getId() > 0));
        assertThat(true, is(DAO.rename(categ.getId(), "foobar")));
        
        PreparedStatement ps = CONN.prepareStatement("SELECT `name` FROM " + CATEG_TABLE_NAME + " WHERE `category_id` = ?");
        ps.setInt(1, categ.getId());
        ResultSet rs = ps.executeQuery();
        rs.next();
        
        assertThat(rs.getString("name"), is("foobar"));
    }  
    
    @Test
    public void tree_method_populates_a_recursive_object_graph_of_category_models() throws Exception
    {
        Category categ1 = DAO.save(getValidCategory("1"));
        
        Category categ1_1 = getValidCategory("1_1");
        categ1_1.setParentId(categ1.getId());
        DAO.save(categ1_1);
        
        Category categ1_2 = getValidCategory("1_2");
        categ1_2.setParentId(categ1.getId());
        DAO.save(categ1_2);
        
        Category categ1_3 = getValidCategory("1_3");
        categ1_3.setParentId(categ1.getId());
        DAO.save(categ1_3);
        
        Category categ1_2_1 = getValidCategory("1_2_1");
        categ1_2_1.setParentId(categ1_2.getId());
        DAO.save(categ1_2_1);
        
        Category categ2 = DAO.save(getValidCategory("2"));
        
        Map<Integer, Category> map = DAO.getCategoryWithChildren(0);
        assertThat(map.size(), is(7));
        assertThat(true, is(map.containsKey(categ1_2_1.getId())));
        assertThat(categ1_2_1.getParentId(), is(categ1_2.getId()));
        assertThat(categ1_3.getParentId(), is(categ1.getId()));
        assertThat(0, is(categ2.getParentId()));
        assertThat(0, is(categ1.getParentId()));
        
        Map<Integer, Category> map2 = DAO.getCategoryWithChildren(categ1_2.getId());
        assertThat(2, is(map2.size()));
        assertThat(map2.get(categ1_2.getId()), is(notNullValue()));
        assertThat(map2.get(categ1_2.getId()).getChildren().size(), is(1));
        assertThat(true, is(map2.get(categ1_2.getId()).getChildren().get(0).getId() == categ1_2_1.getId()));
    }
    
    @Test
    public void get_parents_of_a_child_categ_works() throws Exception
    {
        Category categ1 = DAO.save(getValidCategory("1"));
        
        Category categ1_1 = getValidCategory("1_1");
        categ1_1.setParentId(categ1.getId());
        DAO.save(categ1_1);
        
        Category categ1_2 = getValidCategory("1_2");
        categ1_2.setParentId(categ1.getId());
        DAO.save(categ1_2);
        
        Category categ1_3 = getValidCategory("1_3");
        categ1_3.setParentId(categ1.getId());
        DAO.save(categ1_3);
        
        Category categ1_2_1 = getValidCategory("1_2_1");
        categ1_2_1.setParentId(categ1_2.getId());
        DAO.save(categ1_2_1);
        
        Category categ2 = DAO.save(getValidCategory("2"));
        
        assertThat(DAO.getParentIdsOf(categ1_2_1.getId()), hasSize(2));
        assertThat(DAO.getParentIdsOf(categ1_2_1.getId()), contains(categ1_2.getId(), categ1.getId()));
        assertThat(DAO.getParentIdsOf(categ2.getId()), is(emptyIterableOf(Integer.class)));
        assertThat(DAO.getParentIdsOf(categ1_2.getId()), contains(categ1.getId()));
        assertThat(DAO.getParentIdsOf(categ1_3.getId()), contains(categ1.getId()));
    }
    
    @Test(expected = SQLException.class)
    public void get_parents_of_an_non_existent_categ_id_throws() throws Exception
    {
        DAO.getParentIdsOf(1234);
    }
}

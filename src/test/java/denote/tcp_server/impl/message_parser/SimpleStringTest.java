package denote.tcp_server.impl.message_parser;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import denote.Args;
import denote.Config;
import denote.Registry;
import denote.exception.ConfigurationException;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.IMessage;
import denote.message.impl.message.Error;
import denote.message.impl.message.Unknown;

public class SimpleStringTest
{
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    
    private SimpleString st = new SimpleString();
    
    @BeforeClass
    public static void setUpBeforeClass() throws ConfigurationException
    {
        Config config = new Config(new Args(new String[] {}));
        Registry.getInstance().set("config", config);
    }
    
    @Test
    public void unknownMessageIsReturnedOnGarbageMessages()
    {
        assertTrue(new SimpleString().parse("garbage string\r\n") instanceof Unknown);
        assertTrue(new SimpleString().parse("-------") instanceof Unknown);
    }
    
    @Test
    public void errorCommandAreCreatedOnSpacesMessages()
    {
        assertThat(st.parse(""),      instanceOf(Error.class));
        assertThat(st.parse(null),    instanceOf(Error.class));
        assertThat(st.parse("     "), instanceOf(Error.class));
        assertThat(st.parse("\r\n"),  instanceOf(Error.class));
        assertThat(st.parse("\r"),    instanceOf(Error.class));
        assertThat(st.parse("\t"),    instanceOf(Error.class));
    }
    
    @Test
    public void openDbOnSimpleStringCommand()
    {
        IMessage message = st.parse("db:/path/to/db/file.db");
        assertThat(message, instanceOf(IMessage.class));
        assertThat(message.getAction(), instanceOf(Action.OPEN.getClass()));
        assertThat(message.getDomain(), instanceOf(Domain.DB.getClass()));
        assertThat(message.getParams(), is(notNullValue()));
        assertThat("/path/to/db/file.db", is(message.getParams().get("path").getAsString()));
    }
}

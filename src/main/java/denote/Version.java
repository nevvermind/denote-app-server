package denote;

public class Version
{
    public static final int MAJOR = 0;
    public static final int MINOR = 1;
    public static final int RELEASE = 0;
    
    @Override
    public String toString()
    {
        return MAJOR + "." + MINOR + "." + RELEASE;
    }
}

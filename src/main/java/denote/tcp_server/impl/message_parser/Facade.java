package denote.tcp_server.impl.message_parser;

import denote.message.api.IMessage;
import denote.tcp_server.api.message_parser.IMessageParser;

/**
 * Checks if the command is a JSON formatted one or a simple string
 * and uses the appropriate parser. 
 */
public class Facade implements IMessageParser
{
    @SuppressWarnings("unchecked")
    @Override
    public <T extends IMessage> T parse(String rawMessage)
    {
        T message;
        
        // JSON?
        if (rawMessage.startsWith("{") || rawMessage.startsWith("[")) {
            message = (T) new Json().parse(rawMessage);
        } else {
            message = (T) new SimpleString().parse(rawMessage);
        }
        
        return message;
    }
}

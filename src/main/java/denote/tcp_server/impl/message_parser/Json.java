package denote.tcp_server.impl.message_parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import denote.message.impl.message.GenericMessage;
import denote.tcp_server.api.message_parser.IMessageParser;

public class Json implements IMessageParser
{
    @SuppressWarnings("unchecked")
    @Override
    public GenericMessage parse(String rawMessage)
    {
        GenericMessage jsonMsg = getGson().fromJson(rawMessage, GenericMessage.class);
        return jsonMsg;
    }
    
    protected Gson getGson()
    {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson;
    }
}

package denote.tcp_server.impl.message_parser;

import denote.Errors;
import denote.Logger;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.IMessage;
import denote.message.impl.message.Close;
import denote.message.impl.message.Error;
import denote.message.impl.message.GenericMessage;
import denote.message.impl.message.Info;
import denote.message.impl.message.NoteSearch;
import denote.message.impl.message.Ping;
import denote.message.impl.message.Unknown;
import denote.tcp_server.api.message_parser.IMessageParser;
import denote.utils.Strings;

public class SimpleString implements IMessageParser
{
    @SuppressWarnings("unchecked")
    @Override
    public IMessage parse(String rawMessage)
    {
        if (rawMessage == null) {
            Logger.debug("Received a null message");
            return new Error("3", Errors.getError("3"));
        }        
        
        String testMessage = rawMessage;
        boolean isOnlyEmptySpaces = (testMessage.trim().length() == 0);
        
        if (isOnlyEmptySpaces || rawMessage.isEmpty()) {
            Logger.debug("Received an empty message");
            return new Error("4", Errors.getError("4"));
        }
        
        IMessage message = null;
        
        // simple string messages
        if (Action.PING.compare(rawMessage)) {
            message = new Ping();
        } else if (Action.INFO.compare(rawMessage))  {
            message = new Info();
        } else if (Action.CLOSE.compare(rawMessage)) {
            message = new Close();
        } else if (rawMessage.startsWith(NoteSearch.MSG_PREFIX)) {
            message = getNoteSearch(rawMessage);
        } else if (rawMessage.startsWith(Domain.DB.toString())) {
            // this matches "db:<path/to/db>"
            message = new GenericMessage(Domain.DB, Action.OPEN);
            message.getParams().addProperty("path", rawMessage.substring(Domain.DB.toString().length() + 1));
        } else {
            // no match
            message = new Unknown();
        }

        return message;
    }
    
    private IMessage getNoteSearch(String rawMessage) 
    {
        String query = rawMessage.substring(NoteSearch.MSG_PREFIX.length());
        
        if (Strings.isBlank(query)) {
            return new Error("11", Errors.getError("11"));
        }
        IMessage noteSearch = new NoteSearch();
        noteSearch.getParams().addProperty("query", query);
        
        return noteSearch;
    }
}

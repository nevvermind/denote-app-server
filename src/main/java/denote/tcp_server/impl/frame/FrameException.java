package denote.tcp_server.impl.frame;

public class FrameException extends Exception
{
    private static final long serialVersionUID = -6835800133808654277L;
    
    public FrameException(String message)
    {
        super(message);
    }
    
    public FrameException(String message, Throwable cause)
    {
        super(message, cause);
    }
}

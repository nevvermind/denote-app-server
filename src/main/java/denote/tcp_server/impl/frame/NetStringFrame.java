package denote.tcp_server.impl.frame;

import java.io.IOException;
import java.io.Reader;
import java.util.Map;

import denote.Logger;
import denote.response.api.IResponse;
import denote.tcp_server.api.frame.AbstractHeaderFrame;
import denote.tcp_server.api.message_parser.IMessageParser;
import denote.utils.Strings;

/**
 * {@link http://wiki.tcl.tk/15074}
 * {@link http://en.wikipedia.org/wiki/Netstring}
 * 
 * Format: <lengths_in_bytes>:<message>,
 */
public class NetStringFrame extends AbstractHeaderFrame
{
    private final static String HEADER_DELIM = ":";
    private final static char END_DELIM_CHAR = ',';
    
    private final static String MSG_LENGTH_KEY = "length";
    
    public NetStringFrame(Reader reader, IMessageParser messageParser)
    {
        super(reader, messageParser);
    }

    @Override
    protected void doParseHeader(Reader inputReader,
            Map<String, Object> headerData) throws FrameException
    {
        // read one by one, until the limit
        int MAX_LENGTH_HEADER = 30;
        int counter = 0;
        char[] byteRead = new char[1];
        StringBuilder lengthBuilder = new StringBuilder(MAX_LENGTH_HEADER);
        
        try {
            while (inputReader.read(byteRead, 0, 1) != -1 && (counter < MAX_LENGTH_HEADER)) {
                // have we hit the delimiter?
                if (HEADER_DELIM.equals(new String(byteRead))) {
                    headerData.put(MSG_LENGTH_KEY, Integer.parseInt(Strings.ltrim(lengthBuilder.toString())));
                    break; // we're done
                }
                lengthBuilder.append(byteRead); // add the integer chars until we hit ":"
            }
        } catch (IOException e) {
            Logger.error(e);
            throw new FrameException("Error reading header");
        } catch (NumberFormatException e) {
            Logger.error(e);
            throw new FrameException("Error reading message length from header");
        }
    }

    @Override
    protected String getRawMessage(Reader inputReader)
            throws FrameException
    {
        Map<String, Object> headerData = getHeader();
        
        if (!headerData.containsKey(MSG_LENGTH_KEY)) {
            throw new FrameException("Cannot read message as no length was specified");
        }
        
        int messageLength = (int) headerData.get(MSG_LENGTH_KEY);
        char[] message = new char[messageLength - 1]; // add the end delimiter
        
        try {
            // read the message
            int readResult = inputReader.read(message, 0, messageLength - 1);
            
            if (readResult == -1) {
                throw new FrameException("Error while reading the message");
            }
            
            // empty message; valid case
            if (message.length == 0) {
                return convertFromCharArray(new char[] {});
            }
            
            if ((message.length > 0) && message[message.length - 1] != END_DELIM_CHAR) {
                throw new FrameException("Invalid message format. Could not find end delimiter while reading message.");
            }
            
            return convertFromCharArray(message, 0, message.length - 1); // minus the end delimiter
            
        } catch (IOException e) {
            Logger.error(e);
            throw new FrameException("Error reading message");
        }
    }
    
    private String convertFromCharArray(char[] messageBytes)
    {
        return new String(messageBytes);
    }    
    
    private String convertFromCharArray(char[] messageBytes, int startIndex, int lastIndex)
    {
        return new String(messageBytes, startIndex, lastIndex - startIndex);
    }

	@Override
	protected String extractResponseString(IResponse response) 
	{
        StringBuilder netStringResponse = new StringBuilder();
        String rawResponse = response.getResponse();

        // length first
        netStringResponse.append(String.valueOf(rawResponse.length() + 2));
        
        // ':'
        netStringResponse.append(HEADER_DELIM);
        
        // message
        netStringResponse.append(rawResponse);
        
        // end delim
        netStringResponse.append(END_DELIM_CHAR);
        
        return netStringResponse.toString();
	}
}

package denote.tcp_server.impl.frame;

import java.io.Reader;

import denote.Registry;
import denote.tcp_server.api.frame.IFrame;
import denote.tcp_server.api.message_parser.IMessageParser;
import denote.tcp_server.impl.message_parser.Facade;

public class FrameFactory
{
    public static IMessageParser getMessageParser()
    {
        return new Facade();
    }
    
    public static IFrame getFrame(Reader in)
    {
        return new NetStringFrame(in, getMessageParser());
    }    
}

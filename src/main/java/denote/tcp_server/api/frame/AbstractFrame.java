package denote.tcp_server.api.frame;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import denote.Logger;
import denote.Registry;
import denote.response.api.IResponse;
import denote.tcp_server.Server;
import denote.tcp_server.api.message_parser.IMessageParser;
import denote.tcp_server.impl.frame.FrameException;

public abstract class AbstractFrame implements IFrame
{
    private IMessageParser messageParser;
    private Reader reader;
    
    public AbstractFrame(Reader reader, IMessageParser messageParser)
    {
        this.reader = reader;
        this.messageParser = messageParser;
    }
    
    public IMessageParser getMessageParser()
    {
        return messageParser;
    }
    
    public Reader getReader()
    {
        return reader;
    }
    
    public final void writeMessage(IResponse response, OutputStream out) throws FrameException
    {
        String reponseString = extractResponseString(response);
        
        try {
            out.write(reponseString.getBytes(Server.COMM_CHARSET));
            out.flush();
        } catch (UnsupportedEncodingException e) {
            Logger.error(e);
            throw new FrameException("Error writing to output with charset " + Server.COMM_CHARSET);
        } catch (IOException e) {
            Logger.error(e);
            throw new FrameException("Error writing to output");
        }    	
    }
    
    abstract protected String extractResponseString(IResponse response);
}

package denote.tcp_server.api.frame;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import denote.message.api.IMessage;
import denote.tcp_server.api.message_parser.IMessageParser;
import denote.tcp_server.impl.frame.FrameException;

/**
 * Frame with header. E.g. netstring.
 */
public abstract class AbstractHeaderFrame extends AbstractFrame
{
    public AbstractHeaderFrame(Reader reader, IMessageParser messageParser)
    {
        super(reader, messageParser);
    }

    private final Map<String, Object> header = new HashMap<String, Object>();
    private IMessage message;
    
    private boolean isHeaderRead = false;
    private boolean isMessageRead = false;
    
    @Override
    public boolean hasHeader()
    {
        return true;
    }
    
    @Override
    public final Map<String, Object> getHeader() 
        throws FrameException 
    {
        if (!isHeaderRead) {
            // TODO - implement timer? http://stackoverflow.com/a/4978369/394589
            doParseHeader(getReader(), header);
            isHeaderRead = true;
        }
        return header;
    }
    
    @Override
    public final IMessage parseMessage()
        throws FrameException
    {
        if (!isMessageRead) {
            getHeader();
            // TODO - implement timer? http://stackoverflow.com/a/4978369/394589
            message = getMessageParser().parse(getRawMessage(getReader()));
            isMessageRead = true;
        }
        return message;
    }
    
    public void clear()
    {
        header.clear();
        message = null;
        isHeaderRead = false;
        isMessageRead = false;
    }
    
    abstract protected void doParseHeader(Reader inputReader, Map<String, Object> headerData) 
        throws FrameException;
    
    abstract protected String getRawMessage(Reader inputReader) 
            throws FrameException;
}

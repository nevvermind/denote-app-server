package denote.tcp_server.api.frame;

import java.io.OutputStream;
import java.util.Map;

import denote.message.api.IMessage;
import denote.response.api.IResponse;
import denote.tcp_server.impl.frame.FrameException;

/**
 * A frame is a single message unit received from the client.
 *
 * The meaning of "unit" depends on the actual implementation details -
 * it could be an EOF, or - as in case of netstrings - a final comma.
 *
 * Some frame implementation can have header information (like netstrings, do: message length).
 *
 * Implementations should return a IMessage from parseMessage() as
 * soon as they consider they have a full message unit.
 */
public interface IFrame
{
    IMessage parseMessage() throws FrameException;

    boolean hasHeader();

    Map<String, Object> getHeader() throws FrameException;

    void clear();

    void writeMessage(IResponse response, OutputStream out) throws FrameException;
}

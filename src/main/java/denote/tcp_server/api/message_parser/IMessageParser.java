package denote.tcp_server.api.message_parser;

import denote.message.api.IMessage;

public interface IMessageParser
{
    <T extends IMessage> T parse(String rawMessage);
}

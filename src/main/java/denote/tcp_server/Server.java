package denote.tcp_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import denote.Config;
import denote.Logger;
import denote.Registry;
import denote.controller.Controller;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.impl.message.GenericMessage;

public class Server 
{
	public static final int DEFAULT_PORT = 1234;
	
	public static final String COMM_CHARSET = "UTF-8";
	private static final String CFGKEY_SRV_PORT = "srv.port";
	private static final String CFGKEY_SRV_MAX_QUEUE = "srv.max_queue";
	private static final String CFGKEY_SRV_TCP_NO_DELAY = "srv.tcp_no_delay";
	
	private final static Config config = Registry.getConfig(); 
	private ServerSocket srv;
	
	private OutputStream outToClient;
	private Reader inFromClient;
	private Socket sock;
	
	private Controller controller;
	
	public Server(Controller controller)
    {
	    this.controller = controller;
    }
	
	private int getPort()
	{
	    return config.getInt(CFGKEY_SRV_PORT, DEFAULT_PORT);
	}
	
	private int getMaximumQueueLength()
	{
	    return config.getInt(CFGKEY_SRV_MAX_QUEUE, 50);
	}
    
    public void start()
    {
        try {
            srv = new ServerSocket(getPort(), getMaximumQueueLength(), InetAddress.getByName("127.0.0.1"));
            Boolean isTcpNoDelay = config.getBool(CFGKEY_SRV_TCP_NO_DELAY, new Boolean(true));
            Logger.debug("TCP NO DELAY: " + (isTcpNoDelay ? "ON" : "OFF"));
            Logger.info("Port: " + Integer.toString(getPort()));
            sock = srv.accept();
            sock.setTcpNoDelay(isTcpNoDelay);
            try {
                if (!processSocket()) { // we received the close message or something bad happened
                    closeSocket();
                }
            } catch (Exception e) {
                closeSocket(); // close socket but not the server
                Logger.error(e);
            }
        } catch (InterruptedIOException interruptIoException) {
            closeAll();
            System.out.println("Bye");
        } catch (IOException ioException) {
            closeAll();
            Logger.error(ioException);
        }
    }
    
    private boolean processSocket() throws SocketException
    {
        try {
            controller.setIn(new BufferedReader(new InputStreamReader(sock.getInputStream(), COMM_CHARSET)));
            controller.setOut(new PrintStream(sock.getOutputStream()));
            return controller.start();
        } catch (IOException e) {
            Logger.error(e);
            throw new SocketException();
        } catch (Exception e) {
            Logger.error(e);
            throw new SocketException();
        }
    }
    
    private void closeAll()
    {
        closeSocket();
        closeServer();
    }

    private void closeServer()
    {
        if (srv != null && !srv.isClosed()) {
            try {
                new GenericMessage(Domain.DB, Action.CLOSE).execute(); // close DB
                Logger.debug("Closing server");
                srv.close();
            } catch (IOException e) {
                srv = null;
                Logger.error(e);
            }
        }
    }
    
    private void closeSocket()
    {
        outToClient = controller.getOut();
        inFromClient = controller.getIn();
        
        try {
            if (outToClient != null) {
                outToClient.close();
            }
            if (inFromClient != null) {
                inFromClient.close();
            }
            if (sock != null) {
                Logger.debug("Closing socket");
                sock.close();
                sock = null;
            }            
        } catch (IOException e) {
            Logger.error(e);
        }        
    }
}

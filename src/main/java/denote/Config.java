package denote;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import denote.exception.ConfigurationException;
import denote.utils.Strings;

public class Config 
{
	private Properties config = new Properties();
	
	private static final String CONFIG_FILE_PATH = "denote.ini";

	private String configFilePath = CONFIG_FILE_PATH;
	
	public Config(Args args) throws ConfigurationException
	{
		// did the user pass a custom config file path?
		
		String cfgFilePathFromArgs = args.get("config");
		if (Strings.isNotBlank(cfgFilePathFromArgs)) {
			configFilePath = cfgFilePathFromArgs;
		}
		
		createDefaultFileIfNotExists();
		
		InputStream inputStream = null;
		try {
			inputStream = new BufferedInputStream(new FileInputStream(configFilePath));
		} catch (FileNotFoundException e) {
			throw new ConfigurationException("Cannot load " + configFilePath);
		}
		
		try {
			config.load(inputStream);
		} catch (IOException e) {
			throw new ConfigurationException(e);
		}
	}
	
	private Boolean createDefaultFileIfNotExists() throws ConfigurationException
	{
		File configFile = new File(CONFIG_FILE_PATH);
		
		// TODO - what if you can't read it?
		if (configFile.canRead()) {
			return true;
		}
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(CONFIG_FILE_PATH, "UTF-8");
		} catch (FileNotFoundException e) {
			throw new ConfigurationException(e);
		} catch (UnsupportedEncodingException e) {
			throw new ConfigurationException(e);
		}
		
		for (Entry<String, String> config : getDefaultConfigs().entrySet()) {
			writer.println(config.getKey() + "=" + config.getValue());
		}

		writer.close();
		
		return true;
	}
	
	private HashMap<String, String> getDefaultConfigs()
	{
		HashMap<String, String> config = new HashMap<String, String>();
		
//		config.put("debug", "false");
		
		return config;
	}
	
	public Properties getConfig()
	{
		return config;
	}
	
	public Boolean persist()
	{
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(configFilePath);
			config.store(out, null);
		} catch (FileNotFoundException e1) {
			return false;
		} catch (IOException e) {
			return false;
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public Integer getInt(String configKey, int defaultVal)
	{
	     String valueFound = getConfig().getProperty(configKey);
	     
	     if (valueFound == null) {
	         return defaultVal;
	     }
	     
	     try {
	         return Integer.valueOf(valueFound);
        } catch (NumberFormatException e) {}
	     
	    return null;
	}
	
	/**
	 * Returns true is the configuration is string "true", case INsensitive.
	 * For everything else, it return false.
	 * For null, it returns the defaultVal.
	 */
	public Boolean getBool(String configKey, Boolean defaultVal)
	{
        String valueFound = getConfig().getProperty(configKey);
        
        if (valueFound == null) {
            return defaultVal;
        }
        
        if (valueFound.matches("true")) {
            return new Boolean(true);
        }
        
        return new Boolean(false);
	}

	@Override
	public String toString()
	{
		return config.toString();
	}
}

package denote.controller;

import java.io.OutputStream;
import java.io.Reader;

import denote.Logger;
import denote.message.api.IMessage;
import denote.message.impl.message.Close;
import denote.response.api.IResponse;
import denote.response.impl.ErrorResponse;
import denote.tcp_server.api.frame.IFrame;
import denote.tcp_server.impl.frame.FrameException;
import denote.tcp_server.impl.frame.FrameFactory;

public class MainController implements Controller
{
	private Reader in;
	private OutputStream out;
	
	public MainController()
    {
    }
	
	public MainController(Reader in, OutputStream out) 
	{
		setIn(in);
		setOut(out);
	}
	
	public boolean start() throws FrameException
	{
        IFrame frame = FrameFactory.getFrame(getIn());
        IMessage commandMessage = null;
        
        while (true) {
            try {
                Logger.debug("Parsing message...");
                commandMessage = frame.parseMessage(); // TODO - implement timer?
            } catch (FrameException frameException) { // oops
                Logger.error(frameException);
                return false;
            }
            
            if (commandMessage == null) { // client closed the connection ; TODO - what? how?
                Logger.debug("Command message is null. Exiting.");
                return false;
            }
            
            Logger.debug("Got message " + commandMessage.toString());
            IResponse response = commandMessage.execute();
            
            Logger.debug("Sending message: " + response.toString());
            frame.writeMessage(response, getOut());
            
            frame.clear();
            
            // if we got a close message and we OKd it; close stuff
            if ((commandMessage instanceof Close) && !(response instanceof ErrorResponse)) {
                Logger.debug("Close message received.");
                return false;
            }
        }		
	}

    @Override
    public Reader getIn()
    {
        return in;
    }

    @Override
    public OutputStream getOut()
    {
        return out;
    }

    @Override
    public void setOut(OutputStream out)
    {
        this.out = out;
    }

    @Override
    public void setIn(Reader in)
    {
        this.in = in;
    }
}

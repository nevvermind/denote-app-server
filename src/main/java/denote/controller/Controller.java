package denote.controller;

import java.io.OutputStream;
import java.io.Reader;

import denote.tcp_server.impl.frame.FrameException;

public interface Controller
{
    Reader getIn();
    OutputStream getOut();
    void setOut(OutputStream out);
    void setIn(Reader in);
    boolean start() throws FrameException;
}

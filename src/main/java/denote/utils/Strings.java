package denote.utils;

import java.util.Iterator;
import java.util.regex.Pattern;

public class Strings
{
	private final static Pattern LTRIM = Pattern.compile("^\\s+");

	private final static Pattern RTRIM = Pattern.compile("\\s+$");

    /**
     * False on null, empty string or "0".
     * The string is first trimmed of white spaces.
     *
     * @param value
     * @return
     */
    public static boolean isTruthy(String value)
    {
        if (value == null) {
            return false;
        }

        String value2Check = value.trim();

        return !(value2Check.equals("0") || value2Check.isEmpty());

    }

    public static boolean isBlank(String value)
    {
    	return value == null || value.trim().isEmpty();
    }

    public static boolean isNotBlank(String value)
    {
    	return !isBlank(value);
    }

    public static String ltrim(String s)
    {
        return LTRIM.matcher(s).replaceAll("");
    }

    public static String rtrim(String s)
    {
        return RTRIM.matcher(s).replaceAll("");
    }

    public static String join(String delimiter, Iterator<String> iterator)
    {
        StringBuilder result = new StringBuilder();
        while (iterator.hasNext()) {
            result.append(iterator.next());
            if (iterator.hasNext()) {
                result.append(delimiter);
            }
        }
        return result.toString();
    }
}

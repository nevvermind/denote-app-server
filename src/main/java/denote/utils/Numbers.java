package denote.utils;

public class Numbers
{
    /**
     * http://stackoverflow.com/a/1102916/394589
     * 
     * @param str
     * @return
     */
    public static boolean isInteger(String str)
    {
      return str.matches("-?\\d+");
    }
    
    public static boolean isNotNullInteger(String str)
    {
      return str != null && isInteger(str);
    }    
}

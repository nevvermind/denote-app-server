package denote.utils;

public class Time
{
    public static int getUnixTime()
    {
        return (int) (System.currentTimeMillis() / 1000L);
    }
}

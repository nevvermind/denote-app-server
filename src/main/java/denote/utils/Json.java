package denote.utils;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Json
{
    public static String toJson(Map<String, String> map) 
    {
        return new Gson().toJson(map);
    }
    
    public static Map<String, String> toMap(String json)
    {
        Type typeOfHashMap = new TypeToken<Map<String, String>>() { }.getType();
        return new Gson().fromJson(json, typeOfHashMap);        
    }
    
    public static Gson getPrettyGson()
    {
        return new GsonBuilder().setPrettyPrinting().create();
    }

    /**
     * Return a Gson take serializes only exposed members.
     * 
     * @return
     */
    public static Gson getGson()
    {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
}

package denote;

import java.util.HashMap;

import denote.utils.Strings;

public class Errors
{
    private static final HashMap<String, String> errors;
    
    static {
        errors = new HashMap<String, String>();
        errors.put("1", "Unknown command");
        errors.put("2", "Invalid command");
        errors.put("3", "Null message received");
        errors.put("4", "Empty message received");
        errors.put("5", "No DB path was specified");
        errors.put("6", "Could not connect to DB");
        errors.put("7", "Could not close DB connection");
        errors.put("8", "Could not update schema");
        errors.put("9", "Invalid ID number provided");
        errors.put("10", "Invalid date provided");
        errors.put("11", "Invalid search query");
    }
    
    /**
     * Returns the error string based on the passed error code, or an empty string
     * if no such error code was found.
     * 
     * @param errorCode
     * @return
     */
    public static String getError(String errorCode)
    {
        if (Strings.isBlank(errorCode)) {
            throw new IllegalArgumentException("Invalid error code provided");
        }
        
        if (!errors.containsKey(errorCode)) {
            throw new IllegalArgumentException("No error string was found for error code " + errorCode);
        } else {
            return errors.get(errorCode);
        }
    }
}

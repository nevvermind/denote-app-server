package denote.models;

public class ModelException extends Exception
{
    private static final long serialVersionUID = 6540965944259592489L;

    public ModelException()
    {
    }

    public ModelException(String message)
    {
        super(message);
    }

    public ModelException(Throwable cause)
    {
        super(cause);
    }

    public ModelException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ModelException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

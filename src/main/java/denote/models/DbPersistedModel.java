package denote.models;

import denote.db.ModelDaoAbstract;

public interface DbPersistedModel
{
    void save() throws ModelException;

    boolean delete() throws ModelException;

    boolean load(int id) throws ModelException;

    <T extends ModelDaoAbstract<?>> T getResource();
}

package denote.models;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import denote.Logger;
import denote.db.CategoryDao;
import denote.db.ConnectionException;

public class Category implements DbPersistedModel
{
    private int id;
    
    @Expose
    private String name;
    
    private String created_at;
    private Category parent;
    
    @Expose
    @SerializedName("parent_id")
    private int parentId;
    
    @Expose
    private List<Category> children;
    
    public Category()
    {}
    
    public Category(String name)
    {
        setName(name);
    }
    
    public int getId()
    {
        return id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getCreatedAt()
    {
        return created_at;
    }
    
    public void addChild(Category category)
    {
        getChildren().add(category);
    }
    
    public List<Category> getChildren()
    {
        if (children == null) {
            children = new ArrayList<Category>();
        }
        return children;
    }
    
    public int getParentId()
    {
        return parentId;
    }
    
    public Category getParent() throws ModelException
    {
        if (parent == null) {
            Logger.debug("Fetching parent of category ID " + getId());
            if (parentId != 0) {
                try {
                    parent = new Category();
                    getResource().load(parentId, parent);
                } catch (SQLException | ConnectionException e) {
                    throw new ModelException(e);
                }
            } else {
                Logger.debug("No parent ID was set for Category ID" + getId());
                throw new IllegalStateException("No Parent ID was set.");
            }
        }
        return parent;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setCreatedAt(String createdAt)
    {
        this.created_at = createdAt;
    }
    
    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }    
    
    @Override
    public void save() throws ModelException
    {
        try {
            getResource().save(this);
        } catch (Exception e) {
            throw new ModelException(e);
        }
    }

    @Override
    public boolean delete() throws ModelException
    {
        try {
            return getResource().delete(this);
        } catch (SQLException | ConnectionException e) {
            throw new ModelException(e);
        }
    }

    @Override
    public boolean load(int id) throws ModelException
    {
        try {
            return getResource().load(id, this);
        } catch (SQLException | ConnectionException e) {
            throw new ModelException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public CategoryDao getResource()
    {
        return new CategoryDao();
    }
}

package denote.models;

import denote.db.MetaDao;
import denote.exception.NotImplementedException;

public class MetaProperty implements DbPersistedModel
{
    private String property;
    private String value;
    private int id;
    
    public MetaProperty()
    {
    }
    
    public MetaProperty(String property, String value)
    {
        setProperty(property);
        setValue(value);
    }
    
    public String getProperty()
    {
        return property;
    }

    public String getValue()
    {
        return value;
    }

    public int getId()
    {
        return id;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public void save() throws ModelException
    {
        getResource().save(this);
    }

    @Override
    public boolean delete() throws ModelException
    {
        throw new NotImplementedException();
    }

    @Override
    public boolean load(int id) throws ModelException
    {
        throw new NotImplementedException();
    }

    @SuppressWarnings("unchecked")
    @Override
    public MetaDao getResource()
    {
        return new MetaDao();
    }
}

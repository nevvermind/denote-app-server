package denote.models;

import java.sql.SQLException;

import denote.db.ConnectionException;
import denote.db.NoteCategoryDao;

public class NoteCategory implements DbPersistedModel
{
    private int noteId;
    private int categId;
    
    public NoteCategory()
    {
    }
    
    public NoteCategory(int noteId, int categId)
    {
        this.noteId  = noteId;
        this.categId = categId;
    }
    
    @Override
    public void save() throws ModelException
    {
        try {
            getResource().save(this);
        } catch (SQLException | ConnectionException e) {
            throw new ModelException(e);
        }
    }

    @Override
    public boolean delete() throws ModelException
    {
        try {
            return getResource().delete(this);
        } catch (SQLException | ConnectionException e) {
            throw new ModelException(e);
        }
    }

    @Override
    public boolean load(int id)
    {
        throw new IllegalStateException("Cannot load this type of model");
    }

    @SuppressWarnings("unchecked")
    @Override
    public NoteCategoryDao getResource()
    {
        return new NoteCategoryDao();
    }

    public int getNoteId()
    {
        return noteId;
    }

    public int getCategId()
    {
        return categId;
    }

    public void setNoteId(int noteId)
    {
        this.noteId = noteId;
    }

    public void setCategId(int categId)
    {
        this.categId = categId;
    }
}

package denote.models;

import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;

import denote.db.ConnectionException;
import denote.db.Datetime;
import denote.db.NoteDao;
import denote.exception.InvalidDatetimeFormatException;

public class Note implements DbPersistedModel
{
    @Expose
    private int id;
    
    @Expose
    private String title;
    
    @Expose
    private String body;
    
    @Expose
    private ArrayList<Category> categories;
    
    @Expose
    private String created_at;
    
    @Expose
    private String updated_at;
    
    private NoteDao resource;
    
    public Note()
    {
    }
    
    public Note(String title, String body)
    {
        setTitle(title);
        setBody(body);
    }    
    
    public String getTitle()
    {
        return title;
    }

    public String getBody()
    {
        return body;
    }

    public ArrayList<Category> getCategories()
    {
        if (categories == null) {
            categories = new ArrayList<Category>();
        }
        return categories;
    }

    public String getCreatedAt()
    {
        return created_at;
    }

    public String getUpdatedAt()
    {
        return updated_at;
    }

    public int getId()
    {
        return id;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public void addCategory(Category category)
    {
        getCategories().add(category);
    }

    public void setCreatedAt(String createdAt) throws InvalidDatetimeFormatException
    {
        if (!Datetime.validate(createdAt)) {
            throw new InvalidDatetimeFormatException("Invalid created_at date: " + createdAt);
        }
        this.created_at = createdAt;
    }

    public void setUpdatedAt(String updatedAt)  throws InvalidDatetimeFormatException
    {
        if (!Datetime.validate(updatedAt)) {
            throw new InvalidDatetimeFormatException("Invalid update_at date: " + updatedAt);
        }
        this.updated_at = updatedAt;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public void save() throws ModelException
    {
        try {
            getResource().save(this);
        } catch (SQLException | ConnectionException e) {
            throw new ModelException(e);
        }
    }
    
    @Override
    public boolean delete() throws ModelException
    {
        if (getId() > 0) {
            try {
                getResource().delete(this);
            } catch (SQLException | ConnectionException e) {
                throw new ModelException(e);
            }
            return true;
        }
        return false;
        
    }
    
    @Override
    public boolean load(int id) throws ModelException
    {
        if (!(getId() > 0)) {
            try {
                if (!getResource().load(id, this)) {
                    throw new ModelException(String.format("Could not load Note ID %d", id));
                }
                getResource().loadCategories(this);
            } catch (SQLException | ConnectionException e) {
                throw new ModelException(e);
            }
            return true;
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public NoteDao getResource()
    {
        if (resource == null) {
            resource = new NoteDao();
        }
        return resource;
    }
}

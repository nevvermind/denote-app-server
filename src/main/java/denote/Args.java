package denote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Args 
{
	private static final String ARG_PREPEND = "--";
	private static final String ARG_VALUE_DELIM = "=";
	
	private final List<String> args = new ArrayList<String>();
	
	public Args(String[] args) 
	{
		this.args.addAll(Arrays.asList(args));
	}

	/**
	 * If found and has value (--arg=value), the value is returned. 
	 * If the arg exists but it's in a no-value form (--arg), an empty string is returned.
	 * If the arg is not found, null is returned.
	 * 
	 * @param argName
	 * @return
	 */
	public String get(String argName)
	{
		Pattern pattern = Pattern.compile(ARG_PREPEND + argName + "(" + ARG_VALUE_DELIM + "(.+))*");
		
		for (String arg : args) {
			Matcher matcher = pattern.matcher(arg);
			if (matcher.find()) {
				return matcher.group(2) == null ? "" : matcher.group(2);
			}
		}
		return null;
	}
	
	public boolean hasArgs()
	{
		return args.size() > 0;
	}
	
	public boolean hasArg(String arg)
	{
	    return get(arg) != null;
	}
	
	@Override
	public String toString()
	{
		return args.toString();
	}
}

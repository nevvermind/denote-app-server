package denote;

import java.util.Arrays;

public class Main
{
	public static void main(String[] args)
	{
        try {
			App app = new App(args);
			app.start();
		} catch (Exception e) {
		    Logger.error(e);
			System.exit(-1);
		}
	}
}

package denote;

import java.util.HashMap;
import java.util.Map;

import denote.db.MetaDao;

public class Registry 
{
	private final HashMap<String, Object> storage = new HashMap<String, Object>();
	
	private static Registry instance;
	
	private Registry()
	{}
	
	public static Registry getInstance()
	{
		if (instance == null) {
			instance = new Registry();
		}
		
		return instance;
	}
	
	public Object get(String key, Object defaultVal)
	{
		if (storage.containsKey(key)) {
			return storage.get(key);
		}
		return defaultVal;
	}
	
	public Object get(String key)
	{
		return get(key, null);
	}
	
	public void set(String key, Object value)
	{
		storage.put(key, value);
	}
	
	/**
	 * Meta configuration are fetched from the "meta" table and merged into Registry.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static Map<String, String> getMeta()
	{
	    return (Map<String, String>) getInstance().get(MetaDao.REGKEY_META);
	}
	
	public static Config getConfig()
	{
	    Config config = (Config) getInstance().get("config");
	    if (config == null) {
	        throw new IllegalStateException("No config was set in Registry");
	    }
		return config;
	}
}

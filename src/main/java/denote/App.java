package denote;

import denote.controller.MainController;
import denote.exception.ConfigurationException;
import denote.tcp_server.Server;
import denote.tcp_server.impl.frame.FrameException;

public class App
{
    public App(String[] args) throws ConfigurationException
    {
        Config config = new Config(new Args(args));
        Registry.getInstance().set("config", config);
    }
    
    public static void addShutdownHook(Thread thread)
    {
        Runtime.getRuntime().addShutdownHook(thread);        
    }
    
    public void start() throws Exception
    {
        new Server(new MainController()).start();
    }
}

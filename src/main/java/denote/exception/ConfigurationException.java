package denote.exception;

public class ConfigurationException extends Exception 
{
	private static final long serialVersionUID = -2290844306323187642L;
	
	public ConfigurationException()
	{}
	
	public ConfigurationException(String string) 
	{
		super(string);
	}
	
	public ConfigurationException(String string, Throwable e) 
	{
		super(string, e);
	}
	
	public ConfigurationException(Throwable e) 
	{
		super(e.getMessage(), e);
	}	
}

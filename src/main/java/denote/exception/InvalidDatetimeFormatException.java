package denote.exception;

public class InvalidDatetimeFormatException extends Exception
{
    private static final long serialVersionUID = -1190074646706097623L;
    
    public InvalidDatetimeFormatException(String message)
    {
        super(message);
    }
    
    public InvalidDatetimeFormatException()
    {
        super();
    }
}

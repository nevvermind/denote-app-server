package denote.exception;

public class ArgsException extends Exception {

	private static final long serialVersionUID = -2426970852127531672L;
	
	public ArgsException(String message) 
	{
		super(message);
	}
}

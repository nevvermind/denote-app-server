package denote.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import denote.Logger;
import denote.exception.InvalidDatetimeFormatException;
import denote.models.Category;
import denote.models.Note;

public class NoteDao extends ModelDaoAbstract<Note>
{
    public static final String TABLE_NAME = "notes";
    
    @Override
    protected void install() throws DbInstallException
    {
        String ddl = "CREATE TABLE " + getTableName() + " ("
                + "note_id INTEGER PRIMARY KEY, " 
                + "title VARCHAR(255) NOT NULL, "
                + "body TEXT NOT NULL,"
                + "created_at DATETIME NOT NULL,"
                + "updated_at DATETIME NOT NULL"
                + " )";
        
        try {
            getConnection().createStatement().execute(ddl);
            Logger.debug("Installed " + getTableName() + " schema");
        } catch (Exception e) {
            Logger.error(e);
            throw new DbInstallException("Could not install Note schema");
        }        
    }    
    
    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public Note save(Note note) throws SQLException, ConnectionException
    {
        if (note.getId() > 0) { 
            update(note); 
        } else {
            insert(note); 
        }
        return note;
    }

    @Override
    public boolean delete(Note model) throws SQLException, ConnectionException
    {
        int noteId = model.getId();
        if (noteId == 0) {
            throw new IllegalStateException("Cannot delete Note: it has no ID set.");
        }        
        
        String sql = "DELETE FROM " + getTableName() + " WHERE `note_id` = ?";
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, model.getId());
            return ps.executeUpdate() == 1;
        }
    }

    @Override
    public boolean load(int id, Note note) throws SQLException, ConnectionException
    {
        if (id <= 0) {
            throw new IllegalArgumentException("Cannot load Note: invalid Note ID provided");
        }
        
        String sql = "SELECT * FROM " + getTableName() + " WHERE note_id = ?";
        
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next()) {
                    throw new SQLException(String.format("Could not find Note ID %d", id));
                }
                try {
                    note.setCreatedAt(rs.getString("created_at"));
                    note.setUpdatedAt(rs.getString("updated_at"));
                } catch (InvalidDatetimeFormatException dateEx) {
                    Logger.error(dateEx);
                    return false;
                }
                note.setId(rs.getInt("note_id"));
                note.setBody(rs.getString("body"));
                note.setTitle(rs.getString("title"));
                return true;
            }
        }
    }
    
    protected void update(Note note) throws SQLException, ConnectionException
    {
        StringBuilder sql = new StringBuilder();
        
        sql.append("UPDATE " + getTableName() + " SET ")
           .append("title = ?, ")
           .append("body = ?, ")
           .append("updated_at = ? ")
           .append("WHERE note_id = ?");
        
         try (PreparedStatement ps = getConnection().prepareStatement(sql.toString())) {
             
             String updateAtTime = Datetime.getDbCurrentTime();
             
             ps.setString(1, note.getTitle());
             ps.setString(2, note.getBody());
             ps.setString(3, updateAtTime);
             ps.setInt(4, note.getId());
             
             if (ps.executeUpdate() != 1) {
                 throw new ConnectionException("Couldn't update Note model");
             }
             
             // set the updated time only after we make sure the note's updated
             try {
                note.setUpdatedAt(updateAtTime);
            } catch (InvalidDatetimeFormatException e) {} // this will never happen
         }
    }
    
    protected void insert(Note note) throws SQLException, ConnectionException
    {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO " + getTableName() + " VALUES(?,?,?,?,?)");
        
        try (PreparedStatement ps = getConnection().prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
            
            String currentDbTime = Datetime.getDbCurrentTime();
            
            ps.setNull(1, java.sql.Types.INTEGER);
            ps.setString(2, note.getTitle());
            ps.setString(3, note.getBody());
            ps.setString(4, currentDbTime);
            ps.setString(5, currentDbTime);
            
            ps.executeUpdate();
            int lastInsertId = getLastInsertId(ps);
            
            if (lastInsertId < 0) {
                throw new ConnectionException("Invalid last insert id on Note save");
            }
            
            note.setId(lastInsertId);
            try {
                note.setCreatedAt(currentDbTime);
                note.setUpdatedAt(currentDbTime);
            } catch (InvalidDatetimeFormatException e) {} // this will never happen
        }
    }

    public void loadCategories(Note note) throws SQLException, ConnectionException 
    {
        CategoryDao categDao = new CategoryDao();
        for (int categId : new NoteCategoryDao().getCategoryIds(note)) {
            Category category = new Category(); 
            if (!categDao.load(categId, category)) {
                throw new SQLException(String.format("Could not load Note category ID %d", categId));
            }
            note.addCategory(category);
        }
    }
}

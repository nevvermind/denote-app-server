package denote.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import denote.Logger;
import denote.Registry;
import denote.exception.NotImplementedException;
import denote.models.MetaProperty;

public class MetaDao extends ModelDaoAbstract<MetaProperty>
{
    public static final String REGKEY_META = "meta";
    
    public static final String META_KEY_APPVERSION = "app.version";
    public static final String META_KEY_HAS_INSTALLED = "app.has_installed";

    @Override
    public String getTableName()
    {
        return "meta";
    }

    @Override
    protected void install() throws DbInstallException
    {
        // use "IF NOT EXISTS"
        String ddl = "CREATE TABLE IF NOT EXISTS " + getTableName() + " ("
                + "property_id INTEGER PRIMARY KEY, "
                + "property VARCHAR(255) NOT NULL UNIQUE, "
                + "value VARCHAR(255)"
                + " )";         
        
        try {
            getConnection().createStatement().execute(ddl);
            Logger.debug("Installed " + getTableName() + " schema");
            if (!loadMetaIntoConfig()) {
                throw new Exception("Could not load meta information");
            }
        } catch (Exception e) {
            Logger.error(e);
            throw new DbInstallException("Could not install Meta schema");
        }
    }
    
    private boolean loadMetaIntoConfig()
    {
        try {
            ResultSet rs = getConnection().prepareStatement("SELECT * FROM " + getTableName()).executeQuery();
            Map<String, String> meta = new HashMap<String, String>();
            while (rs.next()) {
                meta.put(rs.getString("property"), rs.getString("value"));
            }
            Registry.getInstance().set(REGKEY_META, meta);
            Logger.debug("Loaded meta information into registry");
            return true;
        } catch (SQLException | ConnectionException e) {
            Logger.error(e);
            Logger.debug("Couldn't not load meta information into registry");
            return false;
        }
    }

    @Override
    public MetaProperty save(MetaProperty model)
    {
        try {
            set(model.getProperty(), model.getValue());
        } catch (ConnectionException e) {
            Logger.error(e);
        }
        return model;
    }

    @Override
    public boolean delete(MetaProperty model)
    {
        throw new NotImplementedException();
    }

    @Override
    public boolean load(int id, MetaProperty model)
    {
        throw new UnsupportedOperationException("Cannot load a Meta property by ID. Use get() instead.");
    }
    
    public String get(String property) throws ConnectionException
    {
        try {
            PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM " + getTableName() + " WHERE property_id = ?");
            ps.setString(1, property);
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                return rs.getString("value");
            }
        } catch (SQLException e) {
            Logger.error(e);
        }
        
        return null;
    }
    
    private boolean set(String property, String value) throws ConnectionException
    {
        Logger.debug("Updating property \"" + property + "\" with value \"" + value + "\"");
        try {
            String sql = "INSERT OR REPLACE INTO " + getTableName() + "(property, value) VALUES(?,?)";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, property);
            ps.setString(2, value);
            int res = ps.executeUpdate();
            return res == 1 ? true : false;
        } catch (SQLException e) {
            Logger.error(e);
            return false;
        }
    }

}

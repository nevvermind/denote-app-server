package denote.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Datetime
{
    public static final String DB_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    
    private static final int LEAP_STEP = 4;
    
    private final static String REGEX_YEAR = "((18|19|20|21)\\d\\d)";
    private final static String REGEX_MONTH = "(0[1-9]|1[012])";
    private final static String REGEX_DAY = "(0[1-9]|[12][0-9]|3[01])";
    private final static String REGEX_HOUR = "(0[0-9]|1[0-9]|2[0-3])";
    private final static String REGEX_MINUTES = "([0-5][0-9])";
    private final static String REGEX_SECONDS = REGEX_MINUTES;
    
    private final static Pattern FORMAT = Pattern.compile(
        "^" + REGEX_YEAR + "-" + REGEX_MONTH + "-" + REGEX_DAY + "\\s" + REGEX_HOUR + ":" + REGEX_MINUTES + ":" + REGEX_SECONDS + "$"
    );

    /**
     * http://stackoverflow.com/a/18252071/394589
     * 
     * @param dateTimeString2Check
     * @return
     */
    public static boolean validate(final String dateTimeString2Check)
    {
        if (dateTimeString2Check == null || dateTimeString2Check.trim().isEmpty()) {
            return false;
        }
        
        Matcher matcher = FORMAT.matcher(dateTimeString2Check);
        
        if (matcher.matches()) {
            matcher.reset();
            if (matcher.find()) {
                int day = Integer.parseInt(matcher.group(4));
                int month = Integer.parseInt(matcher.group(3));
                int year = Integer.parseInt(matcher.group(1));

                switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12: return day < 32;
                case 4:
                case 6:
                case 9:
                case 11: return day < 31;
                case 2: 
                    return isValidFebruaryDay(year, day, dateTimeString2Check);
                default:
                    break;
                }
            }
        }
        return false;
    }
    
    private static boolean isValidFebruaryDay(int year, int day, String actualDateTimeString2Check)
    {
        int modulo100 = year % 100;
        if ((modulo100 == 0 && year % 400 == 0) || (modulo100 != 0 && year % LEAP_STEP == 0)) {
            //its a leap year
            return day < 30 && isValidUsingTimeApi(actualDateTimeString2Check);
        } else {
            return day < 29 && isValidUsingTimeApi(actualDateTimeString2Check);
        }
    }
    
    /**
     * http://stackoverflow.com/a/2151338/394589
     * 
     * @param datetimeString
     * @return
     */
    private static boolean isValidUsingTimeApi(String datetimeString)
    {
        SimpleDateFormat df = new SimpleDateFormat(DB_TIME_FORMAT);
        df.setLenient(false);
        try {
            df.parse(datetimeString);
            return true;
        } catch (ParseException ex) {
            return false;
        }        
    }

    public static String getDbCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat(DB_TIME_FORMAT);
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        return sdf.format(new Date());
    }
}

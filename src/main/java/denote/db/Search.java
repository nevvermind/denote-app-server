package denote.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import denote.Logger;
import denote.exception.InvalidDatetimeFormatException;
import denote.models.Note;
import denote.utils.Strings;

public class Search
{
    private Connection conn;
    
    public Search(Connection conn)
    {
        this.conn = conn;
    }
    
    public ArrayList<Note> find(String query)
    {
        ArrayList<Note> result = new ArrayList<Note>();
        
        if (Strings.isBlank(query)) {
            return result;
        }
        
        try {
            try (PreparedStatement ps = conn.prepareStatement("SELECT * FROM `notes` where `body` LIKE ? OR `title` LIKE ?")) {
                ps.setString(1, "%" + query + "%");
                ps.setString(2, "%" + query + "%");
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Note note = new Note();
                        note.setId(rs.getInt("note_id"));
                        note.setTitle(rs.getString("title"));
                        note.setBody(rs.getString("body"));
                        try {
                            note.setCreatedAt(rs.getString("created_at"));
                            note.setUpdatedAt(rs.getString("updated_at"));
                        } catch (InvalidDatetimeFormatException e) {
                            Logger.error(e);
                            return result; // don't do much stuff; bail asap
                        }
                        result.add(note);
                    }
                }
            }
        } catch (SQLException e) {
            Logger.error(e);
        }
        
        
        return result;
    }

}

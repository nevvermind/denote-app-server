package denote.db;

public class ConnectionException extends Exception
{
    private static final long serialVersionUID = 2560789360231981145L;

    public ConnectionException(String message)
    {
        super(message);
    }
}

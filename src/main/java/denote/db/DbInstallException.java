package denote.db;

public class DbInstallException extends Exception
{
    private static final long serialVersionUID = -6653334950240329474L;

    public DbInstallException(String message)
    {
        super(message);
    }
}

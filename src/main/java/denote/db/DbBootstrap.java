package denote.db;

import java.sql.SQLException;

import denote.Logger;
import denote.Registry;
import denote.Version;
import denote.message_handlers.DbHandler;
import denote.models.MetaProperty;
import denote.utils.Strings;

/**
 * Updates or creates the SCHEMA of the database.
 * Creates tables and makes incremental changes based on versions.
 */
public class DbBootstrap
{
    public static void prepareDb() throws DbInstallException
    {
        enableFkContraints();
        
        // the meta table first
        new MetaDao().install();
        
        try {
            if (needsInstall()) {
                Logger.debug("Installing tables");
                installTables();
            }
        } catch (ConnectionException e) {
            Logger.error(e);
            throw new DbInstallException("Error installing tables");
        }
    }
    
    private static boolean needsInstall() throws ConnectionException
    {
        return !Strings.isTruthy(Registry.getMeta().get(MetaDao.META_KEY_HAS_INSTALLED));
    }
    
    /**
     * @link https://www.sqlite.org/foreignkeys.html#fk_enable
     */
    private static void enableFkContraints() throws DbInstallException
    {
        try {
            DbHandler.getConnection().createStatement().execute("PRAGMA foreign_keys = ON");
            Logger.debug("FK support is enabled for this connection");
        } catch (SQLException e) {
            Logger.error(e);
            throw new DbInstallException("Could not enable FK support for this connection");
        }
    }
    
    private static void installTables() throws DbInstallException
    {   
        try {
            new NoteDao().install();
            new CategoryDao().install();
            new NoteCategoryDao().install();
        
            new MetaProperty(MetaDao.META_KEY_HAS_INSTALLED, "1").save();
            new MetaProperty(MetaDao.META_KEY_APPVERSION, new Version().toString()).save();
        } catch (Exception e) {
            Logger.error(e);
            throw new DbInstallException("Could not set meta-property");
        }
    }
}

package denote.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import denote.Logger;
import denote.message_handlers.DbHandler;
import denote.models.DbPersistedModel;

public abstract class ModelDaoAbstract<T extends DbPersistedModel>
{
    private Connection connect() throws ConnectionException
    {
        Connection conn = DbHandler.getConnection();
        if (conn == null) {
            throw new ConnectionException("Cannot connect to DB");
        }
        return conn;
    }
    
    protected final Connection getConnection() throws ConnectionException
    {
        return connect();
    }
    
    protected int getLastInsertId(PreparedStatement ps)
    {
        try {
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }    
        } catch (Exception e) {
            Logger.error(e);
        }
        return -1;
    }

    abstract public String getTableName();
    
    abstract protected void install() throws DbInstallException;    
    
    abstract public T save(T model) throws SQLException, ConnectionException;
    
    abstract public boolean delete(T model) throws SQLException, ConnectionException;
    
    abstract public boolean load(int id, T model) throws SQLException, ConnectionException;
}
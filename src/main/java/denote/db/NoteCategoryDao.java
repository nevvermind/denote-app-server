package denote.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import denote.Logger;
import denote.models.Note;
import denote.models.NoteCategory;

public class NoteCategoryDao extends ModelDaoAbstract<NoteCategory>
{
    public NoteCategoryDao()
    {
    }

    @Override
    public String getTableName()
    {
        return "note_category";
    }

    @Override
    protected void install() throws DbInstallException
    {
        String ddl = "CREATE TABLE " + getTableName() 
                + " ("
                + "`note_id` INTEGER, "
                + "`category_id` INTEGER, "
                + "`order` INTEGER NOT NULL DEFAULT 0, "
                + "PRIMARY KEY (`note_id`, `category_id`), "
                + "UNIQUE (`note_id`, `category_id`, `order`), "
                + "FOREIGN KEY (`note_id`) REFERENCES " + new NoteDao().getTableName() + "(`note_id`) ON DELETE CASCADE,"
                + "FOREIGN KEY (`category_id`) REFERENCES " + new CategoryDao().getTableName() + "(`category_id`) ON DELETE CASCADE"
                + ")";
        
        try {
            getConnection().createStatement().execute(ddl);
            Logger.debug("Installed " + getTableName() + " schema");
        } catch (Exception e) {
            Logger.error(e);
            throw new DbInstallException("Could not install NoteCategory schema");
        }
    }

    /**
     * If we have a UNIQUE constraint violation, we are almost surely trying to "update" the relationship,
     * because the combination is too improbable to make it wrong. So use the IGNORE conflict algorithm.
     */
    @Override
    public NoteCategory save(NoteCategory model) throws SQLException, ConnectionException
    {
        String sql = "INSERT OR IGNORE INTO " + getTableName() + " (`note_id`, `category_id`, `order`) VALUES(?,?,?)";
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, model.getNoteId());
            ps.setInt(2, model.getCategId());
            ps.setInt(3, getNextOrder(model.getNoteId(), model.getCategId()));
            ps.executeUpdate();
        }
        return model;
    }

    @Override
    public boolean delete(NoteCategory model) throws SQLException, ConnectionException
    {
        throw new UnsupportedOperationException("The association is removed via FK constraint mechanism");
    }

    @Override
    public boolean load(int id, NoteCategory model) throws SQLException, ConnectionException
    {
        throw new UnsupportedOperationException("Cannot load an association");
    }
    
    public ArrayList<Integer> getCategoryIds(Note note) throws SQLException, ConnectionException
    {
        String sql = "SELECT `category_id` FROM " + getTableName() + " WHERE `note_id` = ? ORDER BY `order` ASC";
        ArrayList<Integer> categs = new ArrayList<Integer>();
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, note.getId());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    categs.add(rs.getInt("category_id"));
                }
            }
        }
        return categs;
    }
    
    public int getNextOrder(int noteId, int categId) throws SQLException, ConnectionException
    {
        String sql = "SELECT MAX(`order`) FROM " + getTableName() + " WHERE `note_id` = ? AND `category_id` = ?";
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, noteId);
            ps.setInt(1, categId);
            int nextOrder = 0;
            try (ResultSet rs = ps.executeQuery()) {
                nextOrder = rs.getInt(1) == 0 ? 0 : rs.getInt(1) + 1;
            }
            return nextOrder;
        }
    }

    public void clearNoteAssociations(Note note) throws SQLException, ConnectionException
    {
        Logger.debug("Clearing note-category associations for Note ID " + note.getId());
        String sql = "DELETE FROM " + getTableName() + " WHERE `note_id` = ?";
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, note.getId());
            ps.executeUpdate();
        }
    }
}

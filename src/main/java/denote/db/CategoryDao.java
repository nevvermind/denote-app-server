package denote.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import denote.Logger;
import denote.models.Category;

public class CategoryDao extends ModelDaoAbstract<Category>
{
    @Override
    public String getTableName()
    {
        return "categories";
    }

    @Override
    protected void install() throws DbInstallException
    {
        String ddl = "CREATE TABLE `" + getTableName() 
                + "` ("
                + "`category_id` INTEGER PRIMARY KEY, " 
                + "`parent_id` INTEGER NULL, " 
                + "`name` VARCHAR(255) NOT NULL, "
                + "FOREIGN KEY (`parent_id`) REFERENCES `" + getTableName() + "`(`category_id`)"
                + ")";
        
        try {
            getConnection().createStatement().execute(ddl);
            Logger.debug("Installed " + getTableName() + " schema");
        } catch (Exception e) {
            Logger.error(e);
            throw new DbInstallException("Could not install Category schema");
        }        
    }

    @Override
    public Category save(Category model) throws SQLException, ConnectionException
    {
        int newCategId = save2(model.getName(), (model.getParentId() != 0) ? model.getParentId() : 0);
        model.setId(newCategId);
        return model;
    }

    @Override
    public boolean delete(Category model) throws SQLException, ConnectionException
    {
        int categId = model.getId();
        if (categId == 0) {
            throw new IllegalStateException("Cannot delete Category: it has no ID set.");
        }
        return delete2(categId);
    }

    @Override
    public boolean load(int categoryId, Category category) throws SQLException, ConnectionException
    {
        String sql = "SELECT * FROM " + getTableName() + " WHERE `category_id` = ? LIMIT 1";
        
        PreparedStatement ps = getConnection().prepareStatement(sql);
        ps.setInt(1, categoryId);
        ResultSet rs = ps.executeQuery();
        
        if (!rs.next()) {
            return false;
        }
        
        category.setId(rs.getInt("category_id"));
        category.setName(rs.getString("name"));
        category.setParentId(rs.getInt("parent_id"));
        
        Logger.debug("Category ID " + categoryId + " loaded successfully");
        
        return category.getId() != 0;
    }
    
    /**
     * @param name
     * @param parentId If this is int 0, null will be inserted in the DB.
     * @return
     * @throws ConnectionException 
     * @throws SQLException 
     */
    private int save2(String name, int parentId) throws SQLException, ConnectionException
    {
        String sql = "INSERT INTO " + getTableName() + " (`parent_id`, `name`) VALUES(?,?)";
        PreparedStatement ps = getConnection().prepareStatement(sql);
        if (parentId == 0) {
            ps.setNull(1, Types.INTEGER);
        } else {
            ps.setInt(1, parentId);
        }
        ps.setString(2, name);
        ps.executeUpdate();
        int categId = getLastInsertId(ps);
        return categId > 0 ? categId : 0;
    }
    
    public boolean move(int categId, int parentCategId) throws SQLException, ConnectionException
    {
        String sql = "UPDATE " + getTableName() + " SET `parent_id` = ? WHERE `category_id` = ?";
        PreparedStatement ps = getConnection().prepareStatement(sql);
        ps.setInt(1, parentCategId);
        ps.setInt(2, categId);
        return ps.executeUpdate() == 1;
    }
    
    public boolean rename(int categId, String categNewName) throws SQLException, ConnectionException
    {
        String sql = "UPDATE " + getTableName() + " SET `name` = ? WHERE `category_id` = ?";
        PreparedStatement ps = getConnection().prepareStatement(sql);
        ps.setString(1, categNewName);
        ps.setInt(2, categId);
        return ps.executeUpdate() == 1;
    } 
    
    private boolean delete2(int categId) throws SQLException, ConnectionException
    {
            String sql = "DELETE FROM " + getTableName() + " WHERE `category_id` = ?";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1, categId);
            return ps.executeUpdate() == 1;
    }
    
    private HashMap<Integer, Category> categoryIndexMapping = new HashMap<Integer, Category>();
    
    /**
     * This method returns a map of Categories and not a tree or a logical structure.
     * Instead, the categories, while added in a list, are set with their respective parent or children,
     * so there is a logical structure, but in each category, not in the returned map.
     * 
     * @param categId
     * @return
     * @throws SQLException 
     * @throws ConnectionException 
     */
    public HashMap<Integer, Category> getCategoryWithChildren(int categId) throws SQLException, ConnectionException
    {
        // if the categ is 0, we bind the PS with null, but, in that case, "=" won't work,
        // but "IS NULL" will
        String sql = String.format("WITH RECURSIVE "
                + "categories1 (`category_id`, `parent_id`, `name`, `depth`) AS "
                + "(SELECT *, 0 FROM `categories` WHERE `parent_id` %s ? "
                + "UNION ALL "
                + "SELECT `c`.*, `ct`.`depth` + 1 AS `depth` FROM categories1 ct "
                + "JOIN `categories` c ON (`ct`.`category_id` = `c`.`parent_id`)) "
                + "SELECT * FROM `categories1` ORDER BY `depth` ASC",
                (categId == 0) ? "IS" : "="
                );
        
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            if (categId == 0) {
                ps.setNull(1, Types.INTEGER);
            } else {
                ps.setInt(1, categId);
            }
            
            try (ResultSet rs = ps.executeQuery()) {
                // make sure we always have the category which the caller wanted, even is empty
                Category parentCateg = new Category();
                parentCateg.setId(categId);
                categoryIndexMapping.put(categId, parentCateg);
                
                getChildrenCategoriesFromResultSet(rs);
                
                // we need to reset the field
                HashMap<Integer, Category> temp = categoryIndexMapping;
                categoryIndexMapping = new HashMap<Integer, Category>();
                
                return temp;
            }
        }
    }
    
    private void getChildrenCategoriesFromResultSet(ResultSet rs)
    {
        try {
            if (!rs.next()) {
                return;
            }
            
            int categoryId = rs.getInt("category_id");
            int parentId = rs.getInt("parent_id");
            String name = rs.getString("name");
            
            Category childCateg = new Category();
            childCateg.setId(categoryId);
            childCateg.setParentId(parentId);
            childCateg.setName(name);
            
            Category parentCateg;
            if (!categoryIndexMapping.containsKey(parentId)) {
                parentCateg = new Category();
                parentCateg.setId(parentId);
                categoryIndexMapping.put(parentId, parentCateg);
            } else {
                parentCateg = categoryIndexMapping.get(parentId);
            }
            
            parentCateg.addChild(childCateg);
            categoryIndexMapping.put(categoryId, childCateg);
            
            getChildrenCategoriesFromResultSet(rs);
        
        } catch (SQLException e) {
            Logger.error(e);
        }
    }

    /**
     * 
     * @param categId
     * @return Parent Category IDs, starting with the direct parent.
     * @throws SQLException
     * @throws ConnectionException
     */
    public ArrayList<Integer> getParentIdsOf(int categId) throws SQLException, ConnectionException
    {
        ArrayList<Integer> parentIds = new ArrayList<Integer>();
        String sql = "WITH RECURSIVE categories1 (`category_id`, `parent_id`) AS ("
                + "SELECT `category_id`, `parent_id` FROM `categories` WHERE `category_id` = ? "
                + "UNION ALL "
                + "SELECT `c`.`category_id`, `c`.`parent_id` FROM categories1 ct "
                + "JOIN `categories` c ON (`ct`.`parent_id` = `c`.`category_id`)"
                + ") "
                + "SELECT `category_id` FROM `categories1`";
        
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, categId);
            
            try (ResultSet rs = ps.executeQuery()) {
                // the first row is the actual passed categ; we skip that
                if (!rs.next()) {
                    // no row returned; we received an invalid categ id 
                    throw new SQLException("Cannot fetch Category parents. Invalid Category ID: " + categId);
                }
                
                while (rs.next()) {
                    parentIds.add(rs.getInt("category_id"));
                }
            }
        }
        
        return parentIds;
    }
}

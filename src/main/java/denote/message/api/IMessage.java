package denote.message.api;

import com.google.gson.JsonObject;

import denote.response.api.IResponse;

public interface IMessage
{
    Domain getDomain();

    Action getAction();

    JsonObject getParams();

    IResponse execute();
}

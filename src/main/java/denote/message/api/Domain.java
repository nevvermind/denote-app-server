package denote.message.api;

import com.google.gson.annotations.SerializedName;


public enum Domain
{
    @SerializedName("app")
    APP("app"),

    @SerializedName("note")
    NOTE("note"),

    @SerializedName("category")
    CATEGORY("category"),

    @SerializedName("db")
    DB("db");

   private String value;

   Domain(String value)
   {
       this.value = value;
   }

   public boolean compare(String value2Compare)
   {
       return (value2Compare == null) ? false: value.equals(value2Compare);
   }

   @Override
   public String toString()
   {
      return value;
   }
}

package denote.message.api;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;


public abstract class MessageAbstract implements IMessage
{
    /**
     * Where will this message end up? 
     * 
     * Key: "domain".
     * Mandatory
     * 
     * E.g.: category, info, note etc.
     */
    @Expose private Domain domain;
    
    /**
     * What to do with the domain? 
     * 
     * Key: "action"
     * Mandatory.
     * 
     * E.g.: get, set, move, duplicate, check etc.
     */
    @Expose private Action action;
    
    /**
     * Necessary params.
     * 
     * Key: params
     * Optional
     */
    @Expose private JsonObject params;
    
    /**
     * @param appDomain
     * @param action
     * @param params
     */
    public MessageAbstract(Domain appDomain, Action action, JsonObject params)
    {
        this.domain = appDomain;
        this.action = action;
        this.params = params;
    }
    
    /**
     * @param appDomain
     * @param action
     */
    public MessageAbstract(Domain appDomain, Action action)
    {
        this.domain = appDomain;
        this.action = action;
    }

    public Domain getDomain()
    {
        return domain;
    }

    public Action getAction()
    {
        return action;
    }

    public JsonObject getParams()
    {
        if (params == null) {
            params = new JsonObject();
        }
        return params;
    }
    
    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        
        if (getDomain() == null) {
            result.append("[NO DOMAIN]");
        } else {
            result.append("Domain: " + getDomain());
        }
        
        result.append(" | ");
        
        if (getAction() == null) {
            result.append("[NO ACTION]");
        } else {
            result.append("Action: " + getAction());
        }
        
        result.append(" | Params: " + getParams().toString());
        
        return result.toString();
    }
}

package denote.message.api;

import com.google.gson.annotations.SerializedName;


public enum Action
{
    @SerializedName("ping")
    PING("ping"),

    @SerializedName("close")
    CLOSE("close"),

    @SerializedName("info")
    INFO("info"),

    @SerializedName("open")
    OPEN("open"),

    @SerializedName("save")
    SAVE("save"),

    @SerializedName("rename")
    RENAME("rename"),

    @SerializedName("move")
    MOVE("move"),

    @SerializedName("delete")
    DELETE("delete"),

    @SerializedName("tree")
    TREE("tree"),

    @SerializedName("search")
    SEARCH("search");

   private String value;

   Action(String value)
   {
       this.value = value;
   }

   public boolean compare(String value2Compare)
   {
       return (value2Compare == null) ? false: value.equals(value2Compare);
   }

   @Override
   public String toString()
   {
      return value;
   }
}

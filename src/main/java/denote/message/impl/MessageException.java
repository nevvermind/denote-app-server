package denote.message.impl;

public class MessageException extends Exception
{
    private static final long serialVersionUID = 302168973900366268L;

    public MessageException(String message)
    {
        super(message);
    }
}

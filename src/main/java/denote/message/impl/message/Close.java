package denote.message.impl.message;

import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.MessageAbstract;
import denote.response.api.IResponse;
import denote.response.impl.BooleanResponse;

public class Close extends MessageAbstract
{
    public Close()
    {
        super(Domain.APP, Action.CLOSE);
    }

    /**
     * We'll always assume the Server figured out the client called a close message,
     * and it has closed itself.
     * So we can only send a success message.
     */
    @Override
    public IResponse execute()
    {
        return new BooleanResponse(true);
    }    
}

package denote.message.impl.message;

import com.google.gson.JsonObject;

import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.MessageAbstract;
import denote.message_handlers.CategoryHandler;
import denote.message_handlers.DbHandler;
import denote.message_handlers.NoteHandler;
import denote.response.api.IResponse;
import denote.response.impl.InvalidCommand;

public class GenericMessage extends MessageAbstract
{
    public GenericMessage(Domain appDomain, Action action, JsonObject params)
    {
        super(appDomain, action, params);
    }
    
    public GenericMessage(Domain appDomain, Action action)
    {
        super(appDomain, action);
    }    
    
    @Override
    public IResponse execute()
    {
        switch (getDomain()) {
            case NOTE:
                return new NoteHandler().handle(this);
            case DB:
                return new DbHandler().handle(this);
            case CATEGORY:
                return new CategoryHandler().handle(this);            
            default:
                break;
        }
        
        return new InvalidCommand();
    }
}

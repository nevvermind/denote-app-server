package denote.message.impl.message;

import java.util.ArrayList;

import com.google.gson.reflect.TypeToken;

import denote.db.Search;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.MessageAbstract;
import denote.message_handlers.DbHandler;
import denote.models.Note;
import denote.response.api.IResponse;
import denote.utils.Json;


public class NoteSearch extends MessageAbstract
{
    public static final String MSG_PREFIX = "ns:";
    
    public NoteSearch()
    {
        super(Domain.NOTE, Action.SEARCH);
    }
    
    @Override
    public IResponse execute()
    {
        Search search = new Search(DbHandler.getConnection());
        ArrayList<Note> foundNotes = search.find(getParams().get("query").getAsString());
        java.lang.reflect.Type type = new TypeToken<ArrayList<Note>>() {}.getType();
        return new denote.response.impl.Json(Json.getGson().toJson(foundNotes, type));
    }
}


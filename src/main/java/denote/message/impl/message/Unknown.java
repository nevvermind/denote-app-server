package denote.message.impl.message;

import denote.message.api.MessageAbstract;
import denote.response.api.IResponse;
import denote.response.impl.InvalidCommand;

public class Unknown extends MessageAbstract
{
    public Unknown()
    {
        super(null, null);
    }

    @Override
    public IResponse execute()
    {
        return new InvalidCommand();
    }
}

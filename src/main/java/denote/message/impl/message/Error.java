package denote.message.impl.message;

import denote.message.api.MessageAbstract;
import denote.response.api.IResponse;
import denote.response.impl.ErrorResponse;

public class Error extends MessageAbstract
{
    private String message = "";
    private String errorCode;
    
    public Error(String errorCode, String message)
    {
        super(null, null);
        if (message == null) {
            message = "";
        }
        this.errorCode = errorCode;
        this.message = message;
    }

    @Override
    public IResponse execute()
    {
        if (message == null) {
            return new ErrorResponse(errorCode);
        }
        return new ErrorResponse(errorCode, message);
    }
}

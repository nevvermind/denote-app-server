package denote.message.impl.message;

import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.MessageAbstract;
import denote.response.api.IResponse;
import denote.response.api.Type;
import denote.response.impl.Response;

public class Ping extends MessageAbstract
{
    public Ping()
    {
        super(Domain.APP, Action.PING);
    }
    
    @Override
    public IResponse execute()
    {
        return new Response(Type.STRING, "pong");
    }
}

package denote.message.impl.message;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import denote.Registry;
import denote.Version;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.MessageAbstract;
import denote.message_handlers.DbHandler;
import denote.response.api.IResponse;
import denote.utils.Json;

public class Info extends MessageAbstract
{
    public Info()
    {
        super(Domain.APP, Action.INFO);
    }
    
    @Override
    public IResponse execute()
    {
        Map<String, String> info = new HashMap<String, String>();
        
        // connection info
        info.put("db.is_connected", "0");
        Connection conn = DbHandler.getConnection();
        if (conn != null) {
        	info.put("db.is_connected", "1");
        	info.put("db.path", DbHandler.getDbFilePath());
        }
        
        info.put("cwd", System.getProperty("user.dir"));
        info.put("version", new Version().toString());
        
        // TODO - add more info
        
        Gson gson = new Gson();

        return new denote.response.impl.Json(gson.toJson(info));
    }
}

package denote.message_handlers;

import java.sql.SQLException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import denote.Logger;
import denote.Registry;
import denote.db.CategoryDao;
import denote.db.ConnectionException;
import denote.message.api.IMessage;
import denote.models.Category;
import denote.response.api.Type;
import denote.response.impl.BooleanResponse;
import denote.response.impl.InvalidCommand;
import denote.response.impl.Response;
import denote.utils.Json;
import denote.utils.Strings;

public class CategoryHandler implements IMessageHandler
{
    private IMessage message;

    @SuppressWarnings("unchecked")
    @Override
    public Response handle(IMessage message)
    {
        this.message = message;
        
        switch (message.getAction()) {
            case SAVE:
                return save();
            case TREE:
                return tree();                
            default:
                break;
        }
        
        return new InvalidCommand();
    }

    private Response tree()
    {
        JsonObject params = message.getParams();
        if (params.has("parent_id") && !params.get("parent_id").isJsonPrimitive()) {
            return new InvalidCommand();
        }
        
        CategoryDao categDao = new CategoryDao();
        HashMap<Integer, Category> categWithChildren = null;
        
        int parentIdNum = (params.has("parent_id")) ? params.get("parent_id").getAsInt() : 0;
        try {
            categWithChildren = categDao.getCategoryWithChildren(parentIdNum);
        } catch (SQLException | ConnectionException e) {
            Logger.error(e);
            return new InvalidCommand(); // TODO - change to error response
        }
        
        Gson gson = Json.getGson();
        
        return new Response(Type.JSON, gson.toJson(categWithChildren.get(parentIdNum).getChildren()));
    }

    private Response save()
    {
        JsonObject params = message.getParams();
        
        if (!params.get("name").isJsonPrimitive() || Strings.isBlank(params.get("name").getAsString())) {
            return new InvalidCommand();
        }
        
        String categoryName = params.get("name").getAsString();
        
        Category category = new Category();
        category.setName(categoryName);
        
        if (params.has("parent_id") && params.get("parent_id").isJsonPrimitive()) {
            category.setParentId(params.get("parent_id").getAsInt());
        }
        
        int newCategId = 0;
        try {
             category.save();
             newCategId = category.getId();
        } catch (Exception e) {
            Logger.error(e);
            return new BooleanResponse(false);
        }
        
        if (newCategId > 0) {
            return new Response(Type.INTEGER, String.valueOf(newCategId));
        }
        
        return new BooleanResponse(false);
    }

}

package denote.message_handlers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Stack;

import com.google.gson.JsonElement;

import denote.App;
import denote.Errors;
import denote.Logger;
import denote.Registry;
import denote.db.DbBootstrap;
import denote.db.DbInstallException;
import denote.message.api.Action;
import denote.message.api.Domain;
import denote.message.api.IMessage;
import denote.message.impl.message.GenericMessage;
import denote.response.impl.BooleanResponse;
import denote.response.impl.ErrorResponse;
import denote.response.impl.InvalidCommand;
import denote.response.impl.Response;

public class DbHandler implements IMessageHandler
{
    public static final String REGKEY_DB = "db";
    public static final String REGKEY_DB_FILE_PATH = "db.file_path";
    protected IMessage message;

    @SuppressWarnings("unchecked")
    @Override
    public Response handle(IMessage message)
    {
        this.message = message;

        switch (message.getAction()) {
            case OPEN:
                return open();
            case CLOSE:
                return close();
            default:
                return new InvalidCommand();
        }
    }

    public Response open()
    {
        JsonElement dbFilePathElem = message.getParams().get("path");

        if (dbFilePathElem == null || !dbFilePathElem.isJsonPrimitive() || dbFilePathElem.getAsString().trim().length() == 0) {
            return new ErrorResponse("4", Errors.getError("4"));
        }

        // try to close any previous connection we previously stored
        close();

        String dbFilePath = dbFilePathElem.getAsString().trim();

        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbFilePath);
            registerShutdownHook();
            Registry.getInstance().set(REGKEY_DB, conn);
            Registry.getInstance().set(REGKEY_DB_FILE_PATH, dbFilePath);
            prepareDb();
            return new BooleanResponse(true);
        } catch (SQLException e) {
            close();
        } catch (DbInstallException dbInstallException) {
            Logger.error(dbInstallException);
            return new ErrorResponse("8");
        }

        return new ErrorResponse("5", Errors.getError("5"));
    }

    /**
     * Call this method only after a successful DB connection has been made.
     * It's safe to add this hook even if no connection was set, though.
     *
     * TODO - on multiple connections, this is registered multiple times
     */
    private void registerShutdownHook()
    {
        App.addShutdownHook(new Thread() {
            public void run() {
               new GenericMessage(Domain.DB, Action.CLOSE).execute();
            }
        });

    }

    public Response close()
    {
        Connection conn = getConnection();
        if (conn != null) {
            Logger.debug("DB connection found. Trying to close it...");
            try {
                conn.close();
                Registry.getInstance().set(REGKEY_DB, null);
                Registry.getInstance().set(REGKEY_DB_FILE_PATH, null);
                Logger.debug("DB connection successfully closed.");
            } catch(SQLException e) {
                Logger.error(e);
                return new ErrorResponse("7");
            }
        }
        return new BooleanResponse(true);
    }

    /**
     * @return
     */
    public static Connection getConnection()
    {
        return (Connection) Registry.getInstance().get(REGKEY_DB);
    }

    public static String getDbFilePath()
    {
        return (String) Registry.getInstance().get(REGKEY_DB_FILE_PATH);
    }

    private void prepareDb() throws DbInstallException
    {
        DbBootstrap.prepareDb();
    }

    public static class Transaction
    {
        private static boolean topTransactionIsStarted = false;

        private static final Stack<Savepoint> savepointsLifo = new Stack<Savepoint>();

        public static int getNestingLevel()
        {
            if (!topTransactionIsStarted) {
                return 0;
            }
            return savepointsLifo.size() + 1;
        }

        public static void startTransaction() throws SQLException
        {
            Logger.debug("Starting transaction at level: " + getNestingLevel());
            if (!topTransactionIsStarted) {
                getConnection().setAutoCommit(false);
                topTransactionIsStarted = true;
            } else {
                savepointsLifo.add(getConnection().setSavepoint());
            }
        }

        public static void commit() throws SQLException
        {
            if (getConnection().getAutoCommit()) {
                throw new IllegalStateException("Cannot commit a transaction in an auto-commit mode. Start a transaction first.");
            }

            if (!topTransactionIsStarted) {
                throw new IllegalStateException("Cannot commit: No transactions pending");
            }

            Logger.debug("Committing transaction at level: " + getNestingLevel());

            if (savepointsLifo.size() > 0) {
                getConnection().releaseSavepoint(savepointsLifo.firstElement());
                savepointsLifo.pop();
            } else {
                getConnection().commit();
                getConnection().setAutoCommit(true);
                topTransactionIsStarted = false;
            }
        }

        public static void rollback() throws SQLException
        {
            if (getConnection().getAutoCommit()) {
                throw new IllegalStateException("Cannot rollback a transaction in an auto-commit mode. Start a transaction first.");
            }

            if (!topTransactionIsStarted) {
                throw new IllegalStateException("Cannot rollback: No transactions pending");
            }

            Logger.debug("Rolling back transaction at level: " + getNestingLevel());

            if (savepointsLifo.size() > 0) {
                getConnection().rollback(savepointsLifo.firstElement());
                savepointsLifo.pop();
            } else {
                getConnection().rollback();
                getConnection().setAutoCommit(true);
                topTransactionIsStarted = false;
            }
        }
    }
}

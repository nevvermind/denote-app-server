package denote.message_handlers;

import denote.message.api.IMessage;
import denote.response.api.IResponse;

public interface IMessageHandler
{
    <T extends IResponse> T handle(IMessage message);
}

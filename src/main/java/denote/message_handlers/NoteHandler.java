package denote.message_handlers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import denote.Logger;
import denote.db.CategoryDao;
import denote.db.ConnectionException;
import denote.db.NoteCategoryDao;
import denote.message.api.IMessage;
import denote.message_params.NoteSaveParams;
import denote.models.Category;
import denote.models.Note;
import denote.models.NoteCategory;
import denote.response.api.Type;
import denote.response.impl.BooleanResponse;
import denote.response.impl.InvalidCommand;
import denote.response.impl.Response;
import denote.utils.Json;

public class NoteHandler implements IMessageHandler
{
    private IMessage message;
    
    @SuppressWarnings("unchecked")
    @Override
    public Response handle(IMessage message)
    {
        this.message = message;
        
        switch (message.getAction()) {
            case SAVE:
                try {
                    DbHandler.Transaction.startTransaction();
                    Response response = saveNote();
                    DbHandler.Transaction.commit();
                    return response;
                } catch (Exception e) {
                    try {
                        DbHandler.Transaction.rollback();
                    } catch (Exception e1) {
                        Logger.error(e1);
                    }
                    Logger.error(e);
                    return new BooleanResponse(false);
                }
            default:
                break;
        }
        
        return new InvalidCommand();
    }
    
    protected Response saveNote() throws Exception
    {
        NoteSaveParams params = new GsonBuilder().create().fromJson(message.getParams(), NoteSaveParams.class);
        params.validate();
        
        Note note = new Note();
        if (params.getId() != null) {
            note.setId(params.getId());
        }
        note.setBody(params.getBody());
        note.setTitle(params.getTitle());
        note.save();
        
        // clear the associations the re-save them anew
        saveCategAssociation(note, getCategoryIds(params));

        return new Response(Type.INTEGER, String.valueOf(note.getId()));
    }
    
    /**
     * @param params
     * @return List of existing or newly created Category IDs 
     * @throws Exception
     */
    private ArrayList<Integer> getCategoryIds(NoteSaveParams params) throws Exception
    {
        ArrayList<Integer> categIds = new ArrayList<Integer>();
        ArrayList<String> newCategsJson = new ArrayList<String>();
        
        if (params.getCategs() == null) {
            return categIds;
        }
        
        Iterator<JsonElement> it = params.getCategs().iterator();
        
        while (it.hasNext()) {
            JsonElement categInfo = it.next();
            if (categInfo.isJsonPrimitive()) {
                categIds.add(categInfo.getAsInt());
            } else if (categInfo.isJsonObject()) {
                newCategsJson.add(categInfo.toString());
            }
        }
        
        CategoryDao categDao = new CategoryDao();
        
        // check DB ids
        for (int categId : categIds) {
            if (!categDao.load(categId, new Category())) {
                throw new Exception("Couldn't find category " + categId);
            }
        }
        
        // save new categs and add to ids
        for (String newCategJson : newCategsJson) {
            Category categ = Json.getGson().fromJson(newCategJson, Category.class);
            categ.save();
            int newCategId = categ.getId();
            if (newCategId == 0) {
                throw new Exception("Could not save Category");
            }
            categIds.add(newCategId);
        }
        
        return categIds;
    }
    
    /**
     * Clear the association and add them anew.
     * 
     * @param note
     * @param categIds
     * @throws SQLException
     * @throws ConnectionException
     */
    private void saveCategAssociation(Note note, ArrayList<Integer> categIds) throws SQLException, ConnectionException
    {
        NoteCategoryDao noteCategDao = new NoteCategoryDao();
        
        noteCategDao.clearNoteAssociations(note);
        
        for (Integer categId : getCleanCategoryIds(categIds)) {
            noteCategDao.save(new NoteCategory(note.getId(), categId));
        }        
    }
    
    /**
     * Keep only leaf Categories.
     * 
     * @param categIds
     * @return
     * @throws ConnectionException 
     * @throws SQLException 
     */
    private ArrayList<Integer> getCleanCategoryIds(ArrayList<Integer> categIds) throws SQLException, ConnectionException
    {
        CategoryDao categDao = new CategoryDao();
        ArrayList<Integer> newCategIds = categIds;
        
        for (Integer categId : categIds) {
            for (Integer parentId : categDao.getParentIdsOf(categId)) {
                // did the client accidentally put the Parent ID in the categ params? remove it
                if (categIds.contains(parentId)) {
                    newCategIds.remove(parentId);
                }
            }
        }
        
        return newCategIds;
    }
}

package denote.message_params;

public class MessageParamsException extends Exception
{
    private static final long serialVersionUID = -337245185891401783L;

    public MessageParamsException()
    {
    }

    public MessageParamsException(String message)
    {
        super(message);
    }

    public MessageParamsException(Throwable cause)
    {
        super(cause);
    }

    public MessageParamsException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MessageParamsException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

package denote.message_params;

import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

/**
 * Example of a valid Note save message:
 * 
 * {
    "domain": "note",
    "action": "save",
    "params": {
        "id": 1 // for update; can be omitted (for a new note)
        "body": "bodddyyyy",
        "title": "title of note",
        "categs": [
            1, 
            5, 
            90,
            {"name": "Categ 0"}, 
            {"name": "Categ 1", "parent_id": null}, 
            {"name": "Categ 2", "parent_id": 5}
        ]
    }
}
 */
public class NoteSaveParams
{
    private Integer id;
    private String body;
    private String title;
    private JsonArray categs;
    
    public void validate() throws MessageParamsException
    {
        validateBody();
        validateTitle();
        validateCategs();
    }

    public Integer getId()
    {
        return id;
    }

    public String getBody()
    {
        return body;
    }

    public String getTitle()
    {
        return title;
    }

    public JsonArray getCategs()
    {
        return categs;
    }
    
    private void validateBody() throws MessageParamsException
    {
        if (getBody() == null || getBody().equals("")) {
            throw new MessageParamsException("No body was provided");
        }
    }

    private void validateTitle() throws MessageParamsException
    {
        if (getTitle() == null || getTitle().equals("")) {
            throw new MessageParamsException("No title was provided");
        }
    }
    
    /**
     * A Note can be saved without specifying the Categ. 
     * That means it's not assigned to any.
     * 
     * Categs must be a JsonArray with at least one element: 
     * JsonPrimitive or JsonObject decodable to a Category.
     * 
     * @throws MessageParamsException
     */
    private void validateCategs() throws MessageParamsException
    {
        if (getCategs() == null) {
            return;
        }
        
        if (getCategs().size() == 0) {
            return;
        }
        
        Iterator<JsonElement> it = getCategs().iterator();
        
        if (it.hasNext()) {
            JsonElement categElem = it.next();
            MessageParamsException categException = new MessageParamsException("A Category can either be specified by a JSON object or by a number");
            
            if (categElem.isJsonPrimitive()) {
                try {
                    // TODO TEST - with "foobar" as a category ID
                    categElem.getAsInt();
                } catch (ClassCastException e) {
                    throw categException;
                }
            }
            
            if (!(categElem.isJsonObject())) {
               throw categException;
            }
        }
    }     
}

package denote;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Logger 
{
	private static LoggerOutput logger = new SystemOutLogger();
	
	public static void info(String message)
	{
		logger.log("[INFO] " + message);
	}
	
	public static void debug(String message)
	{
        if (Registry.getConfig().getBool("debug", false)) {
			logger.log("[DEBUG] " + message);
		}
	}
	
    public static void warn(String message)
    {
        logger.log("[WARNING] " + message);
    }	
	
	public static void error(Throwable e)
	{
	    // http://stackoverflow.com/a/1149712/394589
	    StringWriter sw = new StringWriter();
	    PrintWriter pw = new PrintWriter(sw);
	    e.printStackTrace(pw);
	    logger.log("[ERROR] \n" + sw.toString());
	}
	
	interface LoggerOutput 
	{
        void log(String message);
	}
	
	static class SystemOutLogger implements LoggerOutput
	{
		public void log(String message)
		{
			if (Registry.getConfig().getBool("debug", false)) {
				System.out.println(message);
			}
		}
	}
}

package denote.response.api;

public enum Type
{
    STRING("+"),
    ERROR("-"),
    INTEGER("$"),
    JSON("*"),
    BOOLEAN("#");

    private String value;

    Type(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return value;
    }
}

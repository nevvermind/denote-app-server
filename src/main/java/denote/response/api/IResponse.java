package denote.response.api;

public interface IResponse
{
    /**
     * In case messages need some sort of type indicator.
     * E.g. "ERROR", "+", "-", "*" etc.
     *
     * @return
     */
    Type getType();

    /**
     * The string that will eventually end up being transmitted.
     *
     * @return
     */
    String getResponse();
}

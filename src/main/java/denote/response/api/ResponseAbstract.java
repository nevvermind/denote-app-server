package denote.response.api;

public abstract class ResponseAbstract implements IResponse 
{
    protected String rawResponse;
    private Type type;
    
    public ResponseAbstract(Type type, String rawMessage)
    {
        this.type = type;
        this.rawResponse = rawMessage;
    }

    public String getRawResponse()
    {
        return rawResponse;
    }
    
    @Override
    public Type getType()
    {
        return type;
    }
    
    @Override
    public final String getResponse()
    {
        return getType().toString() + getRawResponse();
    }
    
    @Override
    public final String toString()
    {
        return getResponse();
    }
}

package denote.response.impl;

import denote.response.api.ResponseAbstract;
import denote.response.api.Type;

public class Response extends ResponseAbstract
{
    public Response(Type type, String rawMessage)
    {
        super(type, rawMessage);
    }
}

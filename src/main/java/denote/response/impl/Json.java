package denote.response.impl;

import denote.response.api.ResponseAbstract;
import denote.response.api.Type;

public class Json extends ResponseAbstract
{
    public Json(String rawMessage)
    {
        super(Type.JSON, rawMessage);
    }
}

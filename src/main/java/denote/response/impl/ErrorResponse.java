package denote.response.impl;

import denote.Errors;
import denote.Logger;
import denote.response.api.Type;

public class ErrorResponse extends Response
{
    private String errorCode;
    
    public ErrorResponse(String errorCode, String rawMessage)
    {
        super(Type.ERROR, rawMessage);
        this.errorCode = errorCode;
    }
    
    public ErrorResponse(String errorCode)
    {
        super(Type.ERROR, Errors.getError(errorCode));
        this.errorCode = errorCode;
    }    
    
    @Override
    public String getRawResponse()
    {
        if (errorCode != null) {
            return "[" + errorCode + "] " + super.getRawResponse(); 
        } else {
            Logger.warn("No error code was found for error message: " + super.getRawResponse());
        }
        return super.getRawResponse();
    }
}

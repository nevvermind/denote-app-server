package denote.response.impl;

import denote.response.api.Type;


public class BooleanResponse extends Response
{
    public BooleanResponse(boolean flag)
    {
        super(Type.BOOLEAN, flag ? "1" : "0");
    }
    
    public boolean isTrue()
    {
        return getRawResponse().equals("1");
    }
}

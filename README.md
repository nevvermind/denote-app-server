### Create jar
`mvn clean install`

### Run
`java -jar /Users/<user>/IdeaProjects/denote-app-server/target/server-0.0.1-SNAPSHOT.jar`

### Debug
For debug mode, add a new config to `denote.ini`: `debug=true`.

The config file can be changed by passing a new arg to the JAR: `--config=/path/to/denote/ini/file`.
The file must be of INI format.

### Simple commands (non-JSON):
 - `ping`
 - `close`
 - `db:/path/to/sqlite/db` (opens or creates a new DB; can be used to open a :memory: one: `13:db::memory:,`)
 - `ns:<note search query>` (searches notes)
